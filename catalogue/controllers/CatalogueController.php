<?php

namespace backend\modules\catalogue\controllers;

use backend\modules\catalogue\models\base\Catalogue;
use Yii;
use yii\web\Controller;

/**
 * Class CatalogueController
 * @package backend\modules\catalogue\controllers
 */
class CatalogueController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
            'as AccessBehavior' => [
                'class' => \backend\behaviors\AccessBehavior::class,
            ],
        ];
    }

    /**
     * @param bool $lang
     * @return string
     */
    public function actionIndex($lang = false)
    {
        $data = (object)
        $languages = !empty(Yii::$app->modules['multilang']) ? Yii::$app->modules['multilang']->getAllLang() : "";

        $data->lang = !empty(Yii::$app->modules['multilang']) ?
            Yii::$app->modules['multilang']->getDefaultLang()['url'] :
            (!empty(Yii::$app->params['defLang']) ? Yii::$app->params['defLang'] : "");

        if ($lang) $data->lang = $lang;

        $model = new Catalogue();

        return $this->render('index', [
            'data' => $data,
            'model' => $model,
            'languages' => $languages,

            ]);
    }

}
