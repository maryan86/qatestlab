<?php

namespace backend\modules\catalogue\controllers;

use backend\controllers\base\CmsController;
use backend\modules\catalogue\enums\Status;
use backend\modules\catalogue\models\Specialization;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * SpecializationController implements the CRUD actions for Direction model.
 */
class SpecializationController extends CmsController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
            'as AccessBehavior' => [
                'class' => \backend\behaviors\AccessBehavior::class,
            ],
        ];
    }

    /**
     * Lists all Specialization models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new Specialization();
        $searchModel->load(Yii::$app->request->queryParams);

        $dataProvider = Specialization::getDataProvider(Yii::$app->request->queryParams, $this->lang);

        $postIds = ArrayHelper::map($dataProvider->getModels(), 'id', 'p_id');
        $otherLangModels = Specialization::find()->where(['in', 'p_id', $postIds])->all();
        $otherLangModels = ArrayHelper::map($otherLangModels, 'p_id', 'id', 'lang');

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'otherLangModels' => $otherLangModels,
            'data' => (object)$this->data,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single Direction model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            'data' => (object)$this->data,
        ]);
    }

    /**
     * Creates a new Specialization model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Specialization();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->p_id, 'lang' => $model->lang]);
        }

        return $this->render('create', [
            'model' => $model,
            'data' => (object)$this->data,
        ]);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\StaleObjectException
     */
    public function actionUpdate($id)
    {
        $model = Specialization::get($id, $this->lang);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->p_id, 'lang' => $model->lang]);
        }

        if (Yii::$app->request->isPost) {
            if (Specialization::updateOne(Yii::$app->request->post(), $id)) return $this->redirect(['index', 'lang' => $this->lang]);
        }

        return $this->render('update', [
            'model' => $model,
            'data' => (object)$this->data,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->status = Status::TRASH;
        $model->update();

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return \backend\modules\catalogue\models\base\Catalogue|null
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = Specialization::get($id, $this->lang)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }


    /**
     * @param $id
     * @return array|bool|\yii\db\ActiveQuery
     */
    public function actionDirection($id){
        \Yii::$app->response->format = Response::FORMAT_JSON;
        if($id){
            $list = Specialization::getSpecializationDirection($id)->all();
            $list = ArrayHelper::map($list, 'p_id', 'name');
            if($list) return $list;
        }
        return false;
    }
}
