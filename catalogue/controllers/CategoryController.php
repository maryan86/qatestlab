<?php

namespace backend\modules\catalogue\controllers;

use backend\controllers\base\CmsController;
use backend\modules\catalogue\enums\Status;
use backend\modules\catalogue\models\Category;
use backend\modules\catalogue\models\CategoryFilter;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * Class CategoryController
 * @package backend\modules\catalogue\controllers
 */
class CategoryController extends CmsController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
            'as AccessBehavior' => [
                'class' => \backend\behaviors\AccessBehavior::class,
            ],
        ];
    }

    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new Category();
        $dataProvider = new ActiveDataProvider([
            'query' => Category::find()->where(['lang' => $this->lang])->andWhere(['in', 'status', [Status::PUBLISH, Status::DRAFT]]),
        ]);

        $postIds = ArrayHelper::map($dataProvider->getModels(), 'id', 'p_id');
        $otherLangModels = Category::find()->where(['in', 'p_id', $postIds])->all();
        $otherLangModels = ArrayHelper::map($otherLangModels, 'p_id', 'id', 'lang');

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'otherLangModels' => $otherLangModels,
            'data' => (object)$this->data,
            'model' => $model,
        ]);
    }

    /**
     * Displays a single Direction model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            'data' => (object)$this->data,
            'categoryFilter' => CategoryFilter::find()->where(['catalogue_category_id' => $id])->all(),
        ]);
    }

    /**
     * Creates a new Category model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Category();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if(isset(Yii::$app->request->post('Category')['filter'])) {
                CategoryFilter::updateCategoryFilters($model->id, Yii::$app->request->post('Category')['filter'], $this->lang);
            }
            return $this->redirect(['view', 'id' => $model->p_id, 'lang' => $model->lang]);
        }

        return $this->render('create', [
            'model' => $model,
            'data' => (object)$this->data,
        ]);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\StaleObjectException
     */
    public function actionUpdate($id)
    {
        $model = Category::get($id, $this->lang);
        $modelCategoryFilter = new CategoryFilter();
        $categoryFilter = $modelCategoryFilter::find()->where(['catalogue_category_id' => $model->id])->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            CategoryFilter::updateCategoryFilters($model->id, Yii::$app->request->post('Category')['filter'] ?? [], $this->lang);
            return $this->redirect(['view', 'id' => $model->p_id, 'lang' => $model->lang]);
        }

        if (Yii::$app->request->isPost) {
            if (Category::updateOne(Yii::$app->request->post(), $id)) return $this->redirect(['index', 'lang' => $this->lang]);
        }

        return $this->render('update', [
            'model' => $model,
            'categoryFilter' => $categoryFilter,
            'data' => (object)$this->data,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->status = Status::TRASH;
        $model->update();

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return \backend\modules\catalogue\models\base\Catalogue|null
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = Category::get($id, $this->lang)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /**
     * @param $id
     * @return array|bool|\yii\db\ActiveRecord[]
     */
    public function actionDirection($id){
        Yii::$app->response->format = Response::FORMAT_JSON;
        if($id){
            $cats = Category::getCategoryDirection($id);
            $cats = ArrayHelper::map($cats, 'id', 'name');
            if($cats) return $cats;
        }
        return false;
    }

    /**
     * @param $id
     * @return array|bool|\yii\db\ActiveRecord[]
     */
    public function actionFilter($id){
        Yii::$app->response->format = Response::FORMAT_JSON;
        if($id){
            $cats = CategoryFilter::getCategoryFilter($id);
            $cats = ArrayHelper::map($cats, 'id', 'name');
            if($cats) return $cats;
        }
        return false;
    }
}
