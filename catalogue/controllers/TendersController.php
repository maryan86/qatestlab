<?php

namespace backend\modules\catalogue\controllers;

use backend\controllers\base\CmsController;
use backend\modules\catalogue\enums\StatusTenders;
use backend\modules\catalogue\models\CompanyTenders;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * Class CompaniesController
 * @package backend\modules\catalogue\controllers
 */
class TendersController extends CmsController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
            'as AccessBehavior' => [
                'class' => \backend\behaviors\AccessBehavior::class,
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $tenders =  CompanyTenders::find()->joinWith('specialization')->distinct();
        if(!empty(Yii::$app->request->get('user_id'))) {
            $tenders =  CompanyTenders::find()->joinWith('specialization')->where(['user_id'=>Yii::$app->request->get('user_id')])->distinct();
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $tenders,
            'sort' => [
                'defaultOrder' => ['created_at' => SORT_DESC, 'id' => SORT_DESC]
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'data' => CompanyTenders::getData(),
        ]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionView($id) {
        $model = CompanyTenders::findOne($id);
        return $this->render('view',['model'=>$model, 'data' => CompanyTenders::getData()]);
    }
    /**
     *
     * Delete tenders
     *
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = CompanyTenders::findOne($id);
        $model->status = StatusTenders::TRASH;
        $model->update();
        return $this->redirect(['index']);
    }
    /**
     *
     * Restore user
     *
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionRestore($id)
    {
        $model = CompanyTenders::findOne($id);
        $model->status = StatusTenders::ACTIVE;
        $model->update();
        return $this->redirect(['index']);
    }
}
