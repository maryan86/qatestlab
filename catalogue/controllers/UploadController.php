<?php

namespace backend\modules\catalogue\controllers;

use frontend\modules\users\models\MediaLogo;
use frontend\modules\users\models\MediaPhotos;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\filters\AjaxFilter;


class UploadController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
            'as AccessBehavior' => [
                'class' => \backend\behaviors\AccessBehavior::class,
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logo' => ['POST'],
                    'photos' => ['POST'],
                ],
            ],
            'ajax' => [
                'class' => AjaxFilter::class,
                'only' => ['logo', 'photos']
            ],
        ];
    }

    /**
     * Upload Company Logo Action
     *
     * @return array
     */
    public function actionLogo()
    {
        $model = new MediaLogo();
        if (Yii::$app->request->isPost) {
            $model->file = UploadedFile::getInstanceByName('file');
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($image = $model->upload()) {
                return ['image' => $image];
            } else {
                if (!empty($model->errors) && !empty($model->errors['file'])) {
                    return ['error' => array_shift($model->errors['file'])];

                }
            }
        }
    }

    /**
     * Upload Company gallery photos actions
     *
     * @return array
     */
    public function actionPhotos()
    {
        $model = new MediaPhotos();
        if (Yii::$app->request->isPost) {
            $model->files = UploadedFile::getInstancesByName('files');
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($images = $model->upload()) {
                return ['images' => $images];
            } else {
                if (!empty($model->errors) && !empty($model->errors['files'])) {
                    return ['error' => array_shift($model->errors['files'])];
                }
            }
        }
    }

}
