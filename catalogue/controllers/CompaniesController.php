<?php

namespace backend\modules\catalogue\controllers;

use backend\controllers\base\CmsController;
use backend\modules\catalogue\enums\Status;
use backend\modules\catalogue\enums\StatusCompany;
use backend\modules\catalogue\enums\StatusTenders;
use backend\modules\catalogue\models\Company;
use backend\modules\catalogue\models\CompanyForm;
use backend\modules\catalogue\models\CompanyReviews;
use backend\modules\catalogue\models\CompanySearch;
use backend\modules\catalogue\models\CompanyTenders;
use backend\modules\catalogue\models\Equipment;
use common\models\User;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use backend\modules\catalogue\Module;

/**
 * Class CompaniesController
 * @package backend\modules\catalogue\controllers
 */
class CompaniesController extends CmsController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                    'reset' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
            'as AccessBehavior' => [
                'class' => \backend\behaviors\AccessBehavior::class,
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $model = new Company();
        $companySearch = new CompanySearch();
        $companySearch->load(Yii::$app->request->queryParams);
        if (!empty(Yii::$app->request->get('user_id'))) {
            $companySearch->user_id = Yii::$app->request->get('user_id');
        }
        $dataProvider = $companySearch->search();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'data' => (object)$this->data,
            'model' => $model,
            'modelSearch' => $companySearch,
        ]);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = CompanyForm::findOne($id);

        if ($model) {
            if (Yii::$app->request->post()) {
                if ($model->load(Yii::$app->request->post()) && $model->save()) {
                    return $this->redirect(['index']);
                }
            }

            $equipments = Equipment::find()->where(['lang' => $model->lang, 'status' => Status::PUBLISH])->orderBy('name')->all();

            return $this->render('update', [
                'model' => $model,
                'equipments' => $equipments,
                'data' => (object)$this->data,
            ]);
        }
        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /**
     * @param $id
     * @return string
     */
    public function actionView($id)
    {
        $model = Company::findOne($id);
        $reviews = new ActiveDataProvider([
            'query' => CompanyReviews::find()->joinWith(['company', 'user'])->where(['company_id' => $model->id])->orderBy(['created_at' => SORT_DESC])->distinct(),
            'sort' => false
        ]);
        return $this->render('view', [
            'model' => $model,
            'data' => (object)$this->data,
            'reviews' => $reviews,
        ]);
    }

    /**
     * @param $id
     * @return CompanyForm|bool
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = CompanyForm::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = Company::findOne($id);
        $model->status = StatusCompany::TRASH;
        $model->update();

        $count = Company::find()
            ->where([
                Company::tableName() . '.status' => StatusCompany::VERIFY,
                'user_id' => $model->user_id
            ])
            ->count();

        if (empty($count)) {
            CompanyTenders::updateAll(['status' => StatusTenders::COMPANY_TRASH], ['user_id' => $model->user_id, 'status' => StatusTenders::ACTIVE]);
        }
        return $this->redirect(['index']);
    }

    /**
     * regenerate user password
     * @param $id - companyId
     * @return bool
     * @throws \Throwable
     * @throws \yii\base\Exception
     * @throws \yii\db\StaleObjectException
     */
    public function actionReset($id)
    {
        $model = Company::findOne($id);
        $user = User::findOne($model->user_id);
        $newPassword = Yii::$app->security->generateRandomString(12);
        $user->setPassword($newPassword);
        $user->generateAuthKey();
        if ($user->save()) {
            Yii::$app->mailer->compose('reset_user',
                [
                    'email' => $user->email,
                    'password' => $newPassword,
                    'firstname' => $user->firstname,
                    'slug' => $model->slug,
                ])
                ->setFrom([Yii::$app->params['fromEmail'] => Yii::t("app", "SITE_NAME")])
                ->setTo($user->email)
                ->setCharset('utf8')
                ->setSubject(Module::t("app", "UTEX - The first Ukrainian marketplace of manufacturers of clothes, shoes and textiles!"))
                ->send();
            return true;
        }
        return false;
    }

}
