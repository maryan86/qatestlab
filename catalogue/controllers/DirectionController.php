<?php

namespace backend\modules\catalogue\controllers;

use backend\controllers\base\CmsController;
use backend\modules\catalogue\enums\Status;
use Yii;
use backend\modules\catalogue\models\Direction;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DirectionController implements the CRUD actions for Direction model.
 */
class DirectionController extends CmsController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
            'as AccessBehavior' => [
                'class' => \backend\behaviors\AccessBehavior::class,
            ],
        ];
    }

    /**
     * Lists all Direction models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new Direction();
        $dataProvider = new ActiveDataProvider([
            'query' => Direction::find()
                ->where(['lang' => $this->lang])
                ->andWhere(['in', 'status', [Status::PUBLISH, Status::DRAFT]]),
        ]);

        $postIds = ArrayHelper::map($dataProvider->getModels(), 'id', 'p_id');
        $otherLangModels = Direction::find()->where(['in', 'p_id', $postIds])->all();
        $otherLangModels = ArrayHelper::map($otherLangModels, 'p_id', 'id', 'lang');

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'otherLangModels' => $otherLangModels,
            'data' => (object)$this->data,
            'model' => $model,
        ]);
    }

    /**
     * Displays a single Direction model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            'data' => (object)$this->data,
        ]);
    }

    /**
     * Creates a new Direction model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Direction();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->p_id, 'lang' => $model->lang]);
        }

        return $this->render('create', [
            'model' => $model,
            'data' => (object)$this->data,
        ]);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\StaleObjectException
     */
    public function actionUpdate($id)
    {
        $model = Direction::get($id, $this->lang);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->p_id, 'lang' => $model->lang]);
        }

        if (Yii::$app->request->isPost) {
            if (Direction::updateOne(Yii::$app->request->post(), $id)) return $this->redirect(['index', 'lang' => $this->lang]);
        }

        return $this->render('update', [
            'model' => $model,
            'data' => (object)$this->data,
        ]);
    }

    /**
     * Deletes an existing Direction model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->status = Status::TRASH;
        $model->update();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Direction model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Direction the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Direction::get($id, $this->lang)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
