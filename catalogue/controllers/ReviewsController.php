<?php

namespace backend\modules\catalogue\controllers;

use backend\controllers\base\CmsController;
use backend\modules\catalogue\enums\Status;
use backend\modules\catalogue\enums\StatusCompany;
use backend\modules\catalogue\enums\StatusReviews;
use backend\modules\catalogue\models\Company;
use backend\modules\catalogue\models\CompanyForm;
use backend\modules\catalogue\models\CompanyReviews;
use backend\modules\catalogue\models\Equipment;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * Class ReviewsController
 * @package backend\modules\catalogue\controllers
 */
class ReviewsController extends CmsController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
            'as AccessBehavior' => [
                'class' => \backend\behaviors\AccessBehavior::class,
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $model = new CompanyReviews();
        $query = CompanyReviews::find();
        if (!empty(Yii::$app->request->get('user_id'))) {
            $query = CompanyReviews::find()->where(['user_id' => Yii::$app->request->get('user_id')]);
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['created_at' => SORT_DESC, 'id' => SORT_DESC]
            ],
        ]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'model' => $model,
            'data' => (object)$this->data,
        ]);
    }

    /**
     * Delete reviews
     *
     * @param $id
     * @return \yii\web\Response
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = CompanyReviews::findOne($id);
        $model->status = StatusReviews::TRASH;
        $model->update();
        return $this->redirect(['index']);
    }

    /**
     * Show reviews
     *
     * @param $id
     * @return string
     */
    public function actionView($id)
    {
        $model = CompanyReviews::findOne($id);
        if ($model) {
            return $this->render('view', ['model' => $model,'data' => (object)$this->data,]);
        }
        throw new NotFoundHttpException();
    }

    /**
     * Restored reviews
     *
     * @param $id
     * @return \yii\web\Response
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionRestore($id)
    {
        $model = CompanyReviews::findOne($id);
        $model->status = StatusReviews::ACTIVE;
        $model->update();
        return $this->redirect(['index']);
    }
}
