<?php

namespace backend\modules\catalogue\enums;

use backend\enums\base\EnumBase;
use Yii;

/**
 * @method static Status PUBLISH()
 * @method static Status DRAFT()
 * @method static Status TRASH()
 */

/**
 * Class Status
 * @package backend\modules\catalogue\enums
 */
class Status extends EnumBase
{
    const PUBLISH = 100;
    const DRAFT = 300;
    const TRASH = 500;

    /**
     * @return array
     */
    public static function renderStatus(): array
    {
        return [
            self::PUBLISH => Yii::t('app', 'Published'),
            self::DRAFT => Yii::t('app', 'Draft'),
            self::TRASH => Yii::t('app', 'Delete'),
        ];
    }
}