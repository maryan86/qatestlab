<?php

namespace backend\modules\catalogue\enums;

use backend\enums\base\EnumBase;
use backend\modules\catalogue\Module;
use Yii;

/**
 * Class StatusCompany
 * @package backend\modules\catalogue\enums
 */
class StatusCompany extends EnumBase
{
    const VERIFY = 100;
    const TO_CHECK = 200;
    const NOT_COMPLETED = 300;
    const USER_DEACTIVATED = 400;
    const TRASH = 500;

    /**
     * @return array
     */
    public static function renderStatus(): array
    {
        return [
            self::VERIFY => Module::t('app', 'Status Verify'),
            self::TO_CHECK => Module::t('app', 'Status To Check'),
            self::NOT_COMPLETED => Module::t('app', 'Status Not Completed'),
            self::TRASH => Module::t('app', 'Status Delete'),
        ];
    }

    /**
     * @param $status
     * @return mixed
     */
    public static function getStatus($status)
    {
        $statusArr = [
            self::VERIFY => Yii::t('app', 'Status Verify'),
            self::TO_CHECK => Yii::t('app', 'Status To Check'),
            self::NOT_COMPLETED => Yii::t('app', 'Status Not Completed'),
            self::TRASH => Yii::t('app', 'Status Delete'),
        ];

        return $statusArr[$status];
    }

    /**
     * @param $status
     * @return mixed
     */
    public static function getStatusAdmin($status)
    {
        $statusArr = [
            self::VERIFY => '<span class="label label-success">' . Module::t('app', 'Status Verify') . '</span>',
            self::TO_CHECK => '<span class="label label-primary">' . Module::t('app', 'Status To Check') . '</span>',
            self::NOT_COMPLETED => '<span class="label label-warning">' . Module::t('app', 'Status Not Completed') . '</span>',
            self::TRASH => '<span class="label label-danger">' . Module::t('app', 'Status Delete') . '</span>',
            self::USER_DEACTIVATED => '<span class="label label-danger">' . Module::t('app', 'User Deactivated') . '</span>',
        ];

        return $statusArr[$status];
    }

}