<?php

namespace backend\modules\catalogue\enums;

use backend\enums\base\EnumBase;
use backend\modules\catalogue\Module;
use Yii;

/**
 * Class StatusCompany
 * @package backend\modules\catalogue\enums
 */
class StatusReviews extends EnumBase
{
    const ACTIVE = 100;
    const TRASH = 200;
    const BLOCKED = 300;

    /**
     * @return array
     */
    public static function renderStatus()
    {
        return [
            self::ACTIVE => Yii::t('app', 'Active'),
            self::TRASH => Yii::t('app', 'Delete'),
            self::BLOCKED => Yii::t('app', 'Deleted'),
        ];
    }

    /**
     * @param $status
     * @return mixed
     */
    public static function getStatus($status)
    {
        $statusArr = [
            self::ACTIVE => Yii::t('app', 'Status Active'),
            self::TRASH => Yii::t('app', 'Deleted'),
            self::BLOCKED => Yii::t('app', 'Blocked'),
        ];

        return $statusArr[$status];
    }

    /**
     * @param $status
     * @return mixed
     */
    public static function getStatusAdmin($status)
    {
        $statusArr = [
            self::ACTIVE => '<span class="label label-success">' . Module::t('app', 'Status Active') . '</span>',
            self::TRASH => '<span class="label label-danger">' . Module::t('app', 'Deleted') . '</span>',
            self::BLOCKED => '<span class="label label-danger">' . Module::t('app', 'Blocked') . '</span>',
        ];
        return $statusArr[$status];
    }

}