<?php

namespace backend\modules\catalogue\enums;

use backend\enums\base\EnumBase;
use backend\modules\catalogue\Module;
use Yii;

/**
 * Class StatusCompany
 * @package backend\modules\catalogue\enums
 */
class StatusTenders extends EnumBase
{
    const ACTIVE = 100;
    const TRASH = 200;
    const USER_DEACTIVATED = 300;
    const COMPANY_TRASH = 400;

    /**
     * @return array
     */
    public static function renderStatus()
    {
        return [
            self::ACTIVE => Yii::t('app', 'Active'),
            self::TRASH => Yii::t('app', 'The tender is blocked'),
        ];
    }

    /**
     * @param $status
     * @return mixed
     */
    public static function getStatus($status)
    {
        $statusArr = [
            self::ACTIVE => Yii::t('app', 'Status Active'),
            self::TRASH => Yii::t('app', 'The tender is blocked'),
        ];

        return $statusArr[$status];
    }

    /**
     * @param $status
     * @return mixed
     */
    public static function getStatusAdmin($status)
    {
        $statusArr = [
            self::ACTIVE => '<span class="label label-success">' . Module::t('app', 'Status Active') . '</span>',
            self::TRASH => '<span class="label label-danger">' . Module::t('app', 'The tender has been deleted') . '</span>',
            self::USER_DEACTIVATED => '<span class="label label-danger">' . Module::t('app', 'User Deactivated') . '</span>',
            self::COMPANY_TRASH => '<span class="label label-danger">' . Module::t('app', 'No companies') . '</span>',
        ];
        return $statusArr[$status];
    }

}