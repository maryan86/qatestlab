# Catalogue modules

The control module Catalogue 

# Installation instructions:

* Step 1:
````
In backend/config/main add metod in bootstrap and include modules

        'bootstrap' => [     
            ....,    
            'catalogue'
        ],
        
        'modules' => [
            ...,
            'catalogue' => [
                'class' => 'backend\modules\catalogue\Module',
                     'params' => [
                         'url' => 'catalogue', // url to modules
                     ]
            ]
        ]
````
* Step 2:

````
yii migrate --migrationPath=@backend/modules/menu/migrations

````