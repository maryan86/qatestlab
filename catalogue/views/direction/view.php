<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use \backend\modules\catalogue\Module;
use \common\models\Functions;

/* @var $this yii\web\View */
/* @var $model backend\modules\catalogue\models\Direction */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Module::t('app', 'Catalogue'), 'url' => ['/catalogue', 'lang' => $data->lang]];
$this->params['breadcrumbs'][] = ['label' => Module::t('app', 'Directions'), 'url' => ['index', 'lang' => $data->lang]];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <?php echo \yii\widgets\Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]); ?>

            <div class="panel panel-default">
                <div class="block-title panel-heading">
                    <div class="col-md-9">
                        <?= Html::encode($this->title) ?>
                    </div>

                    <div class="col-md-3">
                        <?php echo $this->render("/catalogue/langLayouts", ['data' => $data, 'id' => $model->p_id]) ?>
                    </div>
                </div>
                <div class="panel-body nopadding-top">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            'p_id',
                            [
                                'attribute' => 'lang',
                                'value' => function ($model) {
                                    return \backend\modules\multilang\models\Lang::find()->where(['url' => $model->lang])->one()->name;
                                },
                                'format' => 'raw',
                            ],
                            'name',
                            [
                                'attribute' => 'status',
                                'value' => function ($model) {
                                    $status = Functions::getStatusText($model->status);
                                    return $status;
                                },
                                'label' => Yii::t('app', 'Status'),
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'created_at',
                                'value' => function ($model) {
                                    return Yii::$app->formatter->asDatetime($model->created_at, 'full');
                                },
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'updated_at',
                                'value' => function ($model) {
                                    return Yii::$app->formatter->asDatetime($model->updated_at, 'full');
                                },
                            ],
                        ],
                    ]) ?>

                    <div class="row col-md-12">
                        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->p_id, 'lang' => $model->lang], ['class' => 'btn btn-primary']) ?>
                        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->p_id, 'lang' => $model->lang], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                'method' => 'post',
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
