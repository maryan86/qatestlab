<?php

use backend\modules\catalogue\enums\Status;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\models\Media;

/* @var $this yii\web\View */
/* @var $model backend\modules\catalogue\models\Direction */
/* @var $form yii\widgets\ActiveForm */
$defPhoto = Yii::getAlias("@web/dist/images/no-image.png");
?>

<div class="direction-form">

    <div class="col-md-12 nopadding">

        <?php $form = ActiveForm::begin(); ?>

        <div class="col-md-6 nopadding">

            <?= $form->field($model, 'lang', [
                'options' => [
                    'class' => 'hide'
                ]
            ])->hiddenInput(['maxlength' => true, 'value' => $data->lang])->label(false) ?>

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        </div>
        <div class="form-group photo-block user-photo col-md-3">
            <div class="inner-block">
                <div class="news-photo"
                     data-url="<?= Url::toRoute(['/media/crop-post', 'type' => 'direction']) ?>">
                    <?php
                    if (!empty($model->image)) {
                        $text = Yii::t('app', 'Edit');
                        $hide = '';
                        echo Html::img(Media::getPreviewPhotoPost($model->image, 1));
                    } else {
                        $text = Yii::t('app', 'Create');
                        $hide = 'hide';
                        echo '<div class="no-image">' . Yii::t('app', 'No Image') . '</div>';
                    }
                    ?>
                </div>
                <a href="#" class="button blue save_news_photo" data-save="<?= Yii::t('app', 'Create') ?>"
                   data-change="<?= Yii::t('app', 'Edit') ?>">
                    <span class="fa fa-desktop"></span>
                    <span id="text-change-news"><?= $text ?></span>
                </a>
                <a href="#" class="button red <?= $hide ?> delete-news-image">
                    <span class="glyphicon glyphicon-remove-circle"></span>
                    <?= Yii::t('app', 'Delete') ?>
                </a>

                <?= $form->field($model, 'image', [
                    'options' => [
                        'id' => 'image-id',
                    ]
                ])->hiddenInput()->label(false) ?>
            </div>
        </div>

        <div class="nopadding col-md-12">
            <div class="col-md-6 settings-form">
                <?php $model->status = $model->isNewRecord ? 100 : $model->status; ?>
                <?= $form->field($model, 'status',
                    ['options' => [
                        'class' => 'select-status col-md-3 nopadding-left'
                    ]])
                    ->dropDownList(
                        Status::renderStatus(),
                        [
                            'prompt' => '',
                            'options' =>
                                [
                                    500 => ['disabled' => $model->isNewRecord ? true : false]
                                ]
                        ]
                    );
                ?>

                <div class="form-group button-group">
                    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'button fa fa-floppy-o blue']) ?>
                </div>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>

</div>

<?= $this->render("//popup/popupMediaInfo", ['action' => 'general']) ?>
