<?php

use yii\helpers\Html;
use yii\helpers\Url;

$languages = !empty(Yii::$app->modules['multilang']) ? Yii::$app->modules['multilang']->getAllLang() : ""; ?>
<div class="lang pull-right" style="text-transform: uppercase">
    <?php if ($languages) : ?>
        <?php foreach ($languages as $key => $language): ?>
            <?= Html::a(
                $language['url'],
                Url::to([Yii::$app->controller->id . "/" . Yii::$app->controller->action->id, 'lang' => $language['url'], 'id' => isset($id) ? $id : null]),
                [
                    'class' => $language['url'] == $data->lang ? 'active' : ''
                ]
            ); ?>
        <?php endforeach ?>
    <?php endif; ?>
</div>
