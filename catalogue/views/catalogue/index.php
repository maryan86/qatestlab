<?php

use yii\helpers\Url;
use backend\modules\catalogue\assets\CatalogueAssets;
use \backend\modules\catalogue\Module;

CatalogueAssets::register($this);
$this->title = Module::t('app', 'AppName');
?>

<div id="page-wrapper" data-lang="<?= $data->lang ?>">
    <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-default">

                <div class="block-title panel-heading">
                    <?php if (!empty($languages)) echo $this->render("/catalogue/langLayouts", ['data' => $data, 'action' => Yii::$app->controller->action->id, 'languages' => $languages]) ?>
                    <div class="col-md-3"><?= $this->title ?></div>
                </div>

                <div class="panel-body">
                    <div class="list-group col-md-6">
                        <button type="button" class="list-group-item list-group-item-action active">
                            <?php echo Yii::t("app", "List of modules") ?>
                        </button>
                        <?php $module = Yii::$app->getModule('catalogue');
                        foreach ($module->params['menu'] as $keyInner => $childHref): ?>
                            <a class="list-group-item list-group-item-action"
                               href="<?= Url::toRoute(['/' . $module->params['url'] . '/' . $childHref]) ?>">
                                <?= Module::t('app', $keyInner) ?>
                            </a>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>