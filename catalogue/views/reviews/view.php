<?php

use yii\helpers\Url;
use yii\helpers\HtmlPurifier;
use yii\helpers\Html;
use yii\widgets\DetailView;
use \backend\modules\catalogue\Module;


$this->title = Module::t('app', 'Company Review') . ': ' . $model->company->name;
$this->params['breadcrumbs'][] = ['label' => Module::t('app', 'Catalogue'), 'url' => ['/catalogue', 'lang' => $data->lang]];
$this->params['breadcrumbs'][] = ['label' => Module::t('app', 'List of Reviews'), 'url' => ['index', 'lang' => $data->lang]];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

?>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <?php echo \yii\widgets\Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]); ?>

            <div class="panel panel-default">
                <div class="block-title panel-heading">
                    <div class="col-md-9">
                        <?= Html::encode($this->title) ?>
                    </div>
                </div>
                <div class="panel-body nopadding-top">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            [
                                'attribute' => 'user_id',
                                'value' => function ($model) {
                                    if ($model->user) {
                                        $user = $model->user;
                                        if ($user) {
                                            $name = $user->firstname . ' ' . $user->lastname;
                                            return Html::a(Html::encode($name), Url::toRoute(['/authusers/view', 'id' => $user->id]), ['data-pjax' => 0]);
                                        } else return Yii::t('app', 'User deleted') . ' (id ' . $model->user_id . ')';
                                    }
                                    return Yii::$app->user->getisGuest();
                                },
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'company_id',
                                'value' => function ($model) use ($data) {
                                    if ($model->company) {
                                        $name = $model->company->name;
                                        return Html::a(Html::encode($name), Url::toRoute(['/catalogue/companies/view', 'id' => $model->company_id]), ['data-pjax' => 0]);
                                    } else return Yii::t('app', 'Company deleted') . ' (id ' . $model->company_id . ')';

                                },
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'review',
                                'value' => function ($model) use ($data) {
                                    if ($model->review) {
                                        return HtmlPurifier::process($model->review);
                                    } else return Yii::t('app', 'Company deleted') . ' (id ' . $model->company_id . ')';
                                },
                                'format' => 'raw',
                            ],

                            [
                                'attribute' => 'created_at',
                                'value' => function ($model) {
                                    return Yii::$app->formatter->asDatetime($model->created_at, 'full');
                                },
                                'format' => 'raw',
                            ],
                        ],
                    ]) ?>

                    <div class="row col-md-12">
                        <?= Html::a('<i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;' . Yii::t('app', 'Go to back'), ['catalogue/reviews'], ['class' => 'btn btn-primary']) ?>
                        <?php if ($model->status == \backend\modules\catalogue\enums\StatusReviews::TRASH) : ?>
                            <?= Html::a('<i class="fa fa-undo" aria-hidden="true"></i>&nbsp;' . Yii::t('app', 'Restore'), ['restore', 'id' => $model->id], [
                                'class' => 'btn btn-success',
                                'data' => [
                                    'confirm' => Module::t('app', 'You are sure you want to recover the data?'),
                                    'method' => 'post',
                                ],
                            ]) ?>
                        <?php else: ?>
                            <?= Html::a('<i class="fa fa-trash" aria-hidden="true"></i>&nbsp;' . Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                                'class' => 'btn btn-danger',
                                'data' => [
                                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                    'method' => 'post',
                                ],
                            ]) ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
