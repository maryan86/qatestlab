<?php

use backend\modules\catalogue\models\Company;
use common\models\User;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\widgets\Pjax;
use \backend\modules\catalogue\Module;
use \backend\modules\catalogue\enums\StatusCompany;
use backend\modules\catalogue\models\CompanyTenders;
use \yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Module::t('app', 'List of Reviews');
$this->params['breadcrumbs'][] = ['label' => Module::t('app', 'Catalogue'), 'url' => ['/catalogue', 'lang' => $data->lang]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="page-wrapper" class="list-companies">
    <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <?php try {
                echo \yii\widgets\Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]);
            } catch (Exception $e) {
            } ?>

            <?php Pjax::begin(['id' => 'table_reviews', 'enablePushState' => false]); ?>

            <div class="panel panel-default">
                <div class="block-title panel-heading">
                    <div class="col-md-2">
                        <?= $this->title; ?>
                    </div>
                </div>

                <div class="panel-body nopadding-top">
                    <?php
                    echo GridView::widget([
                        'dataProvider' => $dataProvider,
                        'layout' => '{items}<div class="col-sm-4 nopadding">{summary}</div>
                            <div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate">{pager}</div>',
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            'id',
                            [
                                'attribute' => 'user_id',
                                'value' => function ($model) {
                                    if ($model->user) {
                                        $user = $model->user;
                                        if ($user) {
                                            $name = $user->firstname . ' ' . $user->lastname;
                                            return Html::a(Html::encode($name), Url::toRoute(['/authusers/view', 'id' => $user->id]), ['data-pjax' => 0]);
                                        } else return Yii::t('app', 'User deleted') . ' (id ' . $model->user_id . ')';
                                    }
                                    return Yii::$app->user->getisGuest();
                                },
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'company_id',
                                'value' => function ($model) use ($data) {
                                    if ($model->company) {
                                        $name = $model->company->name;
                                        return Html::a(Html::encode($name), Url::toRoute(['/catalogue/companies/view', 'id' => $model->company_id]), ['data-pjax' => 0]);
                                    } else return Yii::t('app', 'Company deleted') . ' (id ' . $model->company_id . ')';

                                },
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'review',
                                'value' => function ($model) {
                                    $text = HtmlPurifier::process(strip_tags($model->review));
                                    $text = \common\models\Functions::cutStringSpace($text, 100);
                                    return Html::a($text, Url::toRoute(['/catalogue/reviews/view', 'id' => $model->id]), ['data-pjax' => 0]);
                                },
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'created_at',
                                'value' => function ($model) {
                                    return Yii::$app->formatter->asDatetime($model->created_at, 'medium');
                                },
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'status',
                                'value' => function ($model) {
                                    $status = \backend\modules\catalogue\enums\StatusReviews::getStatusAdmin($model->status);
                                    if ($model->status == \backend\modules\catalogue\enums\StatusReviews::TRASH) {
                                        $status .= Html::a('<span class="fa fa-undo" aria-hidden="true"></span>', Url::toRoute(['restore', 'id' => $model->id]), ['data-pjax' => 0, 'data-confirm' => Module::t('app', 'You are sure you want to recover the data?'), 'data-method' => 'post', 'class' => 'restore']);
                                    }
                                    return $status;
                                },
                                'label' => Yii::t('app', 'Status'),
                                'format' => 'raw',
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'buttons' => [
                                    'update' => function () {
                                        return false;
                                    },
                                ],
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
            <?php Pjax::end(); ?>

        </div>
    </div>
</div>
<style>
    .restore:hover {
        text-decoration: none;
    }

    .restore {
        display: block;
        margin-left: 10px;
    }
</style>