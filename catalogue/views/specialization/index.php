<?php

use backend\modules\catalogue\models\Category;
use backend\modules\catalogue\models\Direction;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Pjax;
use \backend\modules\catalogue\Module;
use common\models\Functions;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Module::t('app', 'Specializations');
$this->params['breadcrumbs'][] = ['label' => Module::t('app', 'Catalogue'), 'url' => ['/catalogue', 'lang' => $data->lang]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <style>
                .filters {
                    display: table-row;
                }
            </style>

            <?php echo \yii\widgets\Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]); ?>

            <?php Pjax::begin(); ?>

            <div class="panel panel-default">
                <div class="block-title panel-heading">
                    <div class="col-md-2">
                        <?= $this->title; ?>
                    </div>
                    <div class="col-md-4">
                        <a href="<?= Url::to(['create', 'lang' => $data->lang]) ?>" data-pjax="0" class="button blue">
                            <span class="glyphicon plus glyphicon-plus"></span><?= Yii::t('app', 'Add') ?>
                        </a>
                    </div>
                    <div class="col-md-6">
                        <?php echo $this->render("/catalogue/langLayouts", ['data' => $data]) ?>
                    </div>
                </div>

                <div class="panel-body nopadding-top">
                    <?php
                    $columns = [
                        ['class' => 'yii\grid\SerialColumn'],
                        'id',
                        [
                            'attribute' => 'name',
                            'value' => function ($model) {
                                return Html::a($model->name, Url::to(['update', 'id' => $model->p_id, 'lang' => $model->lang]), ['data-pjax' => 0]);
                            },
                            'format' => 'raw',
                        ],
                        [
                            'attribute' => 'direction_id',
                            'filter' => ArrayHelper::map(Direction::find()->where(['lang' => $data->lang])->orderBy(['name' => SORT_ASC])->all(), 'id', 'name'),
                            'value' => function ($model) {
                                return $model->direction->name ?? '';
                            },
                            'label' => Module::t('app', 'Specialization direction'),
                            'format' => 'raw',
                        ],
                        [
                            'attribute' => 'category_id',
                            'filter' => ($searchModel->direction_id ? ArrayHelper::map(Category::find()->where(['lang' => $data->lang, 'direction_id' => $searchModel->direction_id])->orderBy(['name' => SORT_ASC])->all(), 'id', 'name') : ArrayHelper::map(Category::find()->where(['lang' => $data->lang])->orderBy(['name' => SORT_ASC])->all(), 'id', 'name')),
                            'value' => function ($model) {
                                return $model->category->name ?? '';
                            },
                            'label' => Module::t('app', 'Specialization category'),
                            'format' => 'raw',
                        ],
                        [
                            'attribute' => 'categoryFilter.name',
                            'label' => Module::t('app', 'Applies to the filter'),
                            'format' => 'raw',
                        ],
                        [
                            'attribute' => 'status',
                            'filter' => false,
                            'value' => function ($model) {
                                $status = Functions::getStatusText($model->status);
                                return $status;
                            },
                            'label' => Yii::t('app', 'Status'),
                            'format' => 'raw',
                        ],
                        [
                            'attribute' => 'created_at',
                            'filter' => false,
                            'value' => function ($model) {
                                return Yii::$app->formatter->asDatetime($model->created_at, 'medium');
                            },
                            'format' => 'raw',
                        ],
                        [
                            'filter' => false,
                            'attribute' => 'updated_at',
                            'value' => function ($model) {
                                return Yii::$app->formatter->asDatetime($model->updated_at, 'medium');
                            },
                            'format' => 'raw',
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'buttons' => [
                                'view' => function ($url, $model) {
                                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', Url::to(['view', 'id' => $model->p_id, 'lang' => $model->lang]), ['data-pjax' => 0]);
                                },
                                'update' => function ($url, $model) {
                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Url::to(['update', 'id' => $model->p_id, 'lang' => $model->lang]), ['data-pjax' => 0]);
                                },
                            ],
                        ],
                    ];
                    ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'layout' => '{items}<div class="col-sm-4 nopadding">{summary}</div>
                        <div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate">{pager}</div>',
                        'filterModel' => $searchModel,
                        'columns' => \common\models\base\CmsModel::getGridColumn($columns, $data, $dataProvider, $otherLangModels),
                    ]); ?>
                </div>

            </div>

            <?php Pjax::end(); ?>

        </div>
    </div>
</div>
