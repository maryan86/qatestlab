<?php

use backend\modules\catalogue\assets\CatalogueAssets;
use backend\modules\catalogue\models\Category;
use backend\modules\catalogue\models\CategoryFilter;
use backend\modules\catalogue\models\Direction;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\catalogue\enums\Status;

CatalogueAssets::register($this);

$categoriesWhere = ['lang' => $data->lang];
if ($model->direction_id) {
    $categoriesWhere['direction_id'] = $model->direction_id;
}

$filterWhere = ['lang' => $data->lang];
if ($model->category_id) {
    $filterWhere['catalogue_category_id'] = $model->category_id;
}

$listCategory = Category::find()
    ->where($categoriesWhere)
    ->andWhere(['status' => Status::PUBLISH])
    ->orderBy('name')
    ->all();

/* @var $this yii\web\View */
/* @var $model backend\modules\catalogue\models\Specialization */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="specialization-form">

    <div class="col-md-12 nopadding">

        <?php $form = ActiveForm::begin(); ?>

        <div class="col-md-6 nopadding">

            <?= $form->field($model, 'lang')->hiddenInput(['maxlength' => true, 'value' => $data->lang])->label(false) ?>

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'direction_id',
                ['options' => [
                    'class' => 'form-group'
                ]])
                ->dropDownList(ArrayHelper::map(Direction::find()
                    ->where(['lang' => $data->lang, 'status' => Status::PUBLISH])
                    ->orderBy('name')
                    ->all(), 'id', 'name'),
                    ['prompt' => ''])
            ?>

            <?= $form->field($model, 'category_id',
                ['options' => [
                    'class' => 'form-group',
                    'style' => $model->isNewRecord || !$listCategory ? 'display:none' : ''
                ]])
                ->dropDownList(ArrayHelper::map($listCategory, 'id', 'name'),
                    ['prompt' => ''])
            ?>

            <?= $form->field($model, 'filter_id',
                ['options' => [
                    'class' => 'form-group',
                    'style' => ($model->isNewRecord || !$model->category_id ? 'display:none' : '')
                ]])
                ->dropDownList(ArrayHelper::map(CategoryFilter::find()
                    ->where($filterWhere)
                    ->orderBy('name')
                    ->all(), 'id', 'name'),
                    ['prompt' => ''])
            ?>
        </div>

        <div class="nopadding col-md-12">
            <div class="col-md-6 settings-form">

                <?php $model->status = $model->isNewRecord ? 100 : $model->status; ?>
                <?= $form->field($model, 'status',
                    ['options' => [
                        'class' => 'select-status col-md-3 nopadding-left'
                    ]])
                    ->dropDownList(
                        Status::renderStatus(),
                        [
                            'prompt' => '',
                            'options' =>
                                [
                                    500 => ['disabled' => $model->isNewRecord ? true : false]
                                ]
                        ]
                    );
                ?>

                <div class="form-group button-group">
                    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'button fa fa-floppy-o blue']) ?>
                </div>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>

</div>
