<?php

use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>"/>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

    <style type="text/css">
        img {
            display: block;
            line-height: 0px !important;
        }
    </style>
</head>
<body>
<table width="100%" bgcolor="dfdede" style="background:#dfdede;" border="0" cellpadding="0" cellspacing="0"
       style="margin:0; padding:0">
    <tr>
        <td>
            <table cellpadding="0" cellspacing="0" width="600" align="center" style="border-spacing: 0px!important; ">
                <tr bgcolor="dfdede" style="background-color:#dfdede;">
                    <td bgcolor="dfdede">
                        <p style="line-height: 0px!important;margin: 0!important;">

                        </p>
                    </td>
                </tr>
                <tr bgcolor="fff" style="background-color:#fff;">
                    <td>
                        <p style="line-height: 0px!important;margin: 0!important;">
                            <img style="line-height: 0px!important; display: block;" width="600" height="92"
                                 src="<?= Yii::$app->request->hostInfo ?>/img/mail/header.jpg">
                        </p>
                    </td>
                </tr>
                <tr bgcolor="fff" style="background-color:#fff;">
                    <td>
                        <p style="text-align: center; line-height: 0px!important;margin: 0!important;padding-top: 40px;padding-bottom: 45px">
                            <img style="line-height: 0px!important; display: inline;" width="179" height="161"
                                 src="<?= Yii::$app->request->hostInfo ?>/img/mail/jumbo.jpg">
                        </p>
                    </td>
                </tr>
                <?php $this->beginBody() ?>
                <?= $content ?>
                <?php $this->endBody() ?>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
<?php $this->endPage() ?>
