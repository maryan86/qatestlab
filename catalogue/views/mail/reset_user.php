<?php
$catalog = Yii::$app->params['basePath'].'/catalog';
$contacts = Yii::$app->params['basePath'].'/contacts';
$tender = Yii::$app->params['basePath'].'/user/add-tender';

$urlCompany = Yii::$app->params['basePath'].'/company/'.$slug;

use backend\modules\catalogue\Module;

?>
    <tr bgcolor="fff" style="background-color:#fff;">
        <td style="vertical-align:top!important;padding-left:33px;padding-right:46px;padding-top:20px;" valign="top">
            <p style="text-align: center; color: #000000;font-family: Arial;font-size: 14px;letter-spacing: 0.3px; margin: 0; margin-bottom: 14px">
                <?php echo Yii::t("app", "Good day") ?>!
            </h1>
            <p style="text-align: center; color: #000000;font-family: Arial;font-size: 14px;letter-spacing: 0.3px; margin: 0; margin-bottom: 14px">
                <?php echo Module::t('app', 'Welcome to the First Ukrainian Marketplace for Manufacturers of Apparel, Shoes and Textiles - UTEX!') ?>
            </p>
            <p style="text-align: center; color: #000000;font-family: Arial;font-size: 14px;letter-spacing: 0.3px; margin: 0; margin-bottom: 14px">
                <?php echo Module::t('app', 'As you are a representative of the garment industry in Ukraine, your company has been included in the UTEX platform business directory.') ?>
            </p>
            <p style="text-align: center; color: #000000;font-family: Arial;font-size: 14px;letter-spacing: 0.3px; margin: 0; margin-bottom: 14px">
                <?php echo Module::t('app', 'You can view this information by clicking the {url} directory.',['url' => '<a href="'.Yii::$app->params['basePath'].'/catalog">'.Yii::$app->params['basePath'].'/catalog</a>']) ?>
            </p>
            <p style="text-align: center; color: #000000;font-family: Arial;font-size: 14px;letter-spacing: 0.3px; margin: 0; margin-bottom: 14px">
                <?php echo Module::t('app', 'You can go to {url} by entering:',['url' => '<a href="'.Yii::$app->params['basePath'].'/user/login ">'.Yii::$app->params['basePath'].'/user/login </a>']) ?>
            </p>
            <p style="text-align: center; color: #000000;font-family: Arial;font-size: 14px;letter-spacing: 0.3px; margin: 0; margin-bottom: 4px">
                <?php echo Yii::t('app', 'E-mail') ?>:
            </p>
            <p style="text-align: center; color: #000000;font-family: Arial;font-size: 14px;letter-spacing: 0.3px; margin: 0; margin-bottom: 20px; font-weight: bold">
                <?php echo $email; ?>
            </p>
            <p style="text-align: center; color: #000000;font-family: Arial;font-size: 14px;letter-spacing: 0.3px; margin: 0; margin-bottom: 4px">
                <?php echo Yii::t('app', 'Password') ?>:
            </p>
            <p style="text-align: center; color: #000000;font-family: Arial;font-size: 14px;letter-spacing: 0.3px; margin: 0; margin-bottom: 4px; font-weight: bold">
                <?php echo $password; ?>
            </p>
            <p style="text-align: center; color: #000000;font-family: Arial;font-size: 14px;letter-spacing: 0.3px; margin: 0; margin-bottom: 14px">
                <?php echo Module::t('app', 'You can find more information about UTEX and its features at {url}.',['url' => '<a href="'.Yii::$app->params['basePath'].'">'.Yii::$app->params['basePath'].'</a>']) ?>
            </p>
        </td>
    </tr>

    <tr  bgcolor="fff" style="background-color:#fff;">
        <td  style="text-align: center; vertical-align:top!important;padding-left:33px;padding-right:46px;padding-top:21px;padding-bottom:21px;" valign="top">
            <?= \yii\helpers\Html::a(Module::t('app', 'Go to company profile'), $urlCompany, ['style' => 'text-align: center; display: inline-block; text-decoration: none; padding: 5px; border: 2px solid #000; color: #000; font-family: Arial;text-transform:uppercase']) ?>
        </td>
    </tr>

    <tr style="border: 1px solid #979797; background-color: #282828;">
        <td style="padding-left: 30px; padding-right: 30px; padding-top: 30px; padding-bottom: 15px">
            <table cellpadding="0" cellspacing="0" width="540" align="center"
                   style="border-spacing: 0px!important; border-bottom: 1px solid #3d3d3d; padding-bottom: 15px">
                <tr>
                    <td style="width: 84px;"><a href="<?= $catalog ?>" target="_blank"
                                                style="color: #e1e1e1; font-family: Arial; font-size: 12px;"><?php echo Yii::t('app', 'Catalog') ?></a>
                    </td>
                    <td style="width: 128px;"><a href="<?= $tender ?>" target="_blank"
                                                 style="color: #e1e1e1; font-family: Arial; font-size: 12px;"><?php echo Yii::t('app', 'Create request') ?></a>
                    </td>
                    <td><a href="<?= $contacts ?>" target="_blank"
                           style="width: 83px; color: #e1e1e1; font-family: Arial; font-size: 12px;"><?php echo Yii::t('app', 'Contacts') ?></a>
                    </td>
                    <td style="width: 150px;"></td>
                    <td style="width: 45px">
                        <?php if (Yii::$app->config->getConstSite("URL_FACEBOOK") != '#'): ?>
                            <a href="<?= Yii::$app->config->getConstSite("URL_FACEBOOK") ?>" style="">
                                <img src="<?= Yii::$app->request->hostInfo ?>/img/mail/facebook.png" alt="">
                            </a>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if (Yii::$app->config->getConstSite("URL_INSTAGRAM") != '#'): ?>
                            <a href="<?= Yii::$app->config->getConstSite("URL_INSTAGRAM") ?>" style="">
                                <img src="<?= Yii::$app->request->hostInfo ?>/img/mail/instagram.png" alt="">
                            </a>
                        <?php endif; ?>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
<?php echo Yii::t('app', 'SIGNUP_EMAIL_FOOTER') ?>