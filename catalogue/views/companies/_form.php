<?php

use backend\modules\catalogue\assets\CatalogueAssets;
use backend\modules\catalogue\enums\StatusCompany;
use backend\modules\catalogue\enums\Status;
use backend\modules\catalogue\Module;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

CatalogueAssets::register($this);

$langCompany = $model->lang;

$languages = !empty(Yii::$app->modules['multilang']) ? Yii::$app->modules['multilang']->getAllLang() : [];

$script = <<< JS
    $(document).on('change', '#companyform-main_direction', function (e) {
        e.preventDefault();
        var directionId = $(this).val();
       
        if(directionId){
            $.post("/control/catalogue/specialization/direction/" + directionId, function () {
                }).done(function (data) {
                    var specializationCategory = $('#companyform-main_specialization');
                    specializationCategory.html('');
                    if (data) {
                        specializationCategory.append($("<option></option>"));
                        $.each(data, function (key, value) {
                            specializationCategory
                                .append($("<option></option>")
                                    .attr("value", key)
                                    .text(value));
                        });
                    }
                    $(specializationCategory).trigger("chosen:updated");
                });
        }
    });
    
    $(document).on('change', '.additional_specialization_select_container select.wwdCustomSelectOnce', function (e) {
        var selfObj = $(this);
        e.preventDefault();
        var directionId = $(this).val();
        $.post("/control/catalogue/specialization/direction/" + directionId, function () {
            }).done(function (data) {
                var specializationCategory = selfObj.parent().next().find('select');
                specializationCategory.html('');
                if (data) {
                    specializationCategory.append($("<option></option>"));
                    $.each(data, function (key, value) {
                        specializationCategory
                            .append($("<option></option>")
                                .attr("value", key)
                                .text(value));
                    });
                }
                $(specializationCategory).trigger("chosen:updated");
            });
    });
    
    $('.company-characteristic').on('click', '.add_specialization', function(e) {
        e.preventDefault();    
        cloneAdditionallySelect();
	});
    
    $('.company-characteristic').on('click', '.removeAdditionalSpec', function() {
        $(this).parents('.additional_specialization_select_container').slideUp(300);
        var obj = $(this);
        setTimeout(function() {
            $(obj).parents('.additional_specialization_select_container').remove();
        }, 300, obj)
    });    
    $('body').on('click', '.sendMailButton', function(e) {
        e.preventDefault();
        var send = confirm("Перегенерувати пароль користувачу та выдправити на пошту?");
        if(send){
            var companyId = $(this).data('company-id');
            $.post("/control/catalogue/companies/reset/" + companyId, function () {
            }).done(function (data) {
                if(data){
                    alert('Пароль перегенеровано.');
                }
            });
        }

    });
    
    function cloneAdditionallySelect() {
        $('.additional_specialization_select_container').find('select.chosen-select').chosen("destroy");
        var clonning = $('.additional_specialization_select_container').first().clone();
        var id = clonning.find('select.wwdCustomSelectOnce').attr('id');
        var idMulti = clonning.find('select.chosen-select').attr('id');
        var length = $('.additional_specialization_select_container').length;
        clonning.find('select option:selected').removeAttr('selected');
        clonning.find('select.wwdCustomSelectOnce').attr('id', id + '_' + length);
        clonning.find('select.chosen-select').attr('id', idMulti + '_' + length);
        clonning.find('select.wwdCustomSelectOnce').attr('name', formName + '[additional_specializations][' + length + '][direction]');
        clonning.find('select.chosen-select').attr('name', formName + '[additional_specializations][' + length + '][specialization][]');
        clonning.find('select.chosen-select').parent().find('input').attr('name', formName + '[additional_specializations][' + length + '][specialization][]');        
        clonning.find('input').attr('name', formName + '[check_additional_specialization][' + length + ']').attr('checked', false).attr('id', 'companyform-check_additional_specialization_'+length);        
        clonning.hide();
        $('.additional_specialization_select_container').last().after(clonning);
        $('.additional_specialization_select_container').last().find('.form-group').last().after('<div class="removeAdditionalSpec"><svg xmlns="http://www.w3.org/2000/svg" width="13" height="17" viewBox="0 0 13 17"><g><g><g><path fill="#FF2D2D" d="M11.972 3.994H1.028V3.16c0-.067.028-.105.093-.105H11.88c.065 0 .093.038.093.105zm-1.057 11.952h-8.83c-.065 0-.093-.028-.093-.095V5.057h9.016v10.794c0 .067-.028.095-.093.095zM8.645 1.053v.949h-4.3v-.949zm3.234.949H9.683V.527A.519.519 0 0 0 9.163 0c-.027 0-.046.01-.055.02A.07.07 0 0 0 9.071 0H3.836c-.287 0-.51.23-.51.527v1.475H1.121C.491 2.002 0 2.509 0 3.16v1.896h.973v10.794c0 .651.482 1.149 1.112 1.149h8.83c.63 0 1.121-.498 1.121-1.15V5.058H13V3.161C13 2.509 12.509 2 11.879 2z"/></g><g><path fill="#FF2D2D" d="M8 6h1v9H8z"/></g><g><path fill="#FF2D2D" d="M6 6h1v9H6z"/></g><g><path fill="#FF2D2D" d="M4 6h1v9H4z"/></g></g></g></svg></div>');
        $('.additional_specialization_select_container').last().slideDown(300);
        $('.additional_specialization').find('.chosen-select').chosen();
    }
JS;
$this->registerJsFile('/control/js/clipboard.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJs($script);

$this->registerJsVar('formName', $model->formName());

$this->registerJs("

    function setTooltip(context, msg) {
        context.tooltip('hide')
            .attr('data-original-title', msg)
            .tooltip('show');
    }

    function hideTooltip(context) {
        setTimeout(function () {
            context.tooltip('hide')
                .attr('data-original-title', context.attr('data-title'))
                .tooltip('fixTitle');
        }, 1000);
    }

    // Clipboard
    var clipboard = new Clipboard('.js-copy-url');
    clipboard.on('success', function (e) {
        setTooltip($(e.trigger), $(e.trigger).attr('data-copied'));
        hideTooltip($(e.trigger));
    });

    clipboard.on('error', function (e) {
        setTooltip($(e.trigger), $(e.trigger).attr('data-failed'));
        hideTooltip($(e.trigger));
    });
",
    \yii\web\View::POS_END);

/* @var $this yii\web\View */
/* @var $model backend\modules\catalogue\models\Direction */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="alert alert-<?= $model->status == StatusCompany::VERIFY ? 'success' : ($model->status == StatusCompany::TO_CHECK ? 'info' : ($model->status == StatusCompany::NOT_COMPLETED ? 'warning' : 'danger')) ?>" role="alert"><?= strip_tags(StatusCompany::getStatusAdmin($model->status)); ?></div>

<div class="company-form">
    <?php $form = ActiveForm::begin(); ?>

    <div class="panel panel-primary">
        <div class="panel-heading"><?php echo Module::t('app', 'General information') ?></div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12 nopadding">
                    <?php if ($languages) : ?>
                        <?php foreach ($languages as $key => $language): ?>
                            <div class="col-md-4">
                                <?= $form->field($model, 'name[' . $language['url'] . ']')->textInput(['value' => $model->translate[$language['url']]->name ?? ''])
                                    ->label(Module::t('app', 'Company Name') . ' [' . $language['name'] . '] ' . Html::img('/control/icons/flags/' . $language["url"] . '.png', ['alt' => $language["name"], 'title' => $language["name"]])) ?>
                            </div>
                        <?php endforeach ?>
                    <?php endif; ?>
                    <div class="col-md-3 margin-top30">
                        <?= $form->field($model, 'verified')->checkbox() ?>
                    </div>
                </div>
            </div>

            <hr>
            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'email')->textInput() ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'edrpou')->textInput() ?>
                </div>
                <div class="col-md-4 margin-top30">
                    <?= $form->field($model, 'associated')->checkbox() ?>
                </div>
            </div>

            <hr>
            <div class="row">
                <div class="col-md-3">
                    <?= $form->field($model, 'site')->textInput() ?>
                </div>
                <div class="col-md-3 margin-top30">
                    <?= $form->field($model, 'check_site')->checkbox() ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <?= $form->field($model, 'year')->textInput() ?>
                </div>
                <div class="col-md-3 margin-top30">
                    <?= $form->field($model, 'check_year')->checkbox() ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <?= $form->field($model, 'personnel_quantity')->textInput() ?>
                </div>
                <div class="col-md-3 margin-top30">
                    <?= $form->field($model, 'check_personnel_quantity')->checkbox() ?>
                </div>
            </div>

            <div class="row">
                <label class="col-md-12"><?= Module::t('app', 'Minimum and maximum order') ?></label>
                <div class="col-md-1">
                    <?= $form->field($model, 'capacity_min')->textInput()->label(false) ?>
                </div>
                <div class="col-md-1">
                    <?= $form->field($model, 'capacity_max')->textInput()->label(false) ?>
                </div>
                <div class="col-md-1">
                    <?= $form->field($model, 'capacity_type')
                        ->dropDownList(Yii::$app->params['capacityType'][$langCompany])
                        ->label(false) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'check_capacity')->checkbox() ?>
                </div>
            </div>

            <hr>
            <div class="row">
                <?php if ($languages) : ?>
                    <?php foreach ($languages as $key => $language): ?>
                        <div class="col-md-6">
                            <?= $form->field($model, 'description[' . $language['url'] . ']')->textarea(["rows" => 4, 'value' => $model->translate[$language['url']]->description ?? ''])
                                ->label(Module::t('app', 'A brief description of the company') . ' [' . $language['name'] . '] ' .
                                    Html::img('/control/icons/flags/' . $language["url"] . '.png', ['alt' => $language["name"], 'title' => $language["name"]])) ?>
                        </div>
                    <?php endforeach ?>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <hr>
    <div class="panel panel-primary">
        <div class="panel-heading"><?php echo Module::t('app', 'Location') ?></div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12 nopadding">
                    <?php if ($languages) : ?>
                        <?php foreach ($languages as $key => $language): ?>
                            <div class="col-md-2">
                                <?= $form->field($model, 'address[' . $language['url'] . ']')->textInput(['value' => $model->translate[$language['url']]->address ?? ''])->label(Module::t('app', 'Address') . ' [' . $language['name'] . '] ' . Html::img('/control/icons/flags/' . $language["url"] . '.png', ['alt' => $language["name"], 'title' => $language["name"]])) ?>
                            </div>
                        <?php endforeach ?>
                        <?php foreach ($languages as $key => $language): ?>
                            <div class="col-md-2">
                                <?= $form->field($model, 'city[' . $language['url'] . ']')->textInput(['value' => $model->translate[$language['url']]->city ?? ''])->label(Module::t('app', 'City') . ' [' . $language['name'] . '] ' . Html::img('/control/icons/flags/' . $language["url"] . '.png', ['alt' => $language["name"], 'title' => $language["name"]])) ?>
                            </div>
                        <?php endforeach ?>
                    <?php endif; ?>
                    <div class="col-md-2">
                        <?= $form->field($model, 'region_pid',
                            ['options' => [
                                'class' => 'form-group'
                            ]])
                            ->dropDownList(ArrayHelper::map(\backend\modules\catalogue\models\Region::find()
                                ->where(['lang' => $langCompany, 'status' => Status::PUBLISH])
                                ->orderBy('name')
                                ->all(), 'id', 'name'),
                                ['prompt' => ''])
                        ?>
                    </div>
                    <div class="col-md-2 margin-top30">
                        <?= $form->field($model, 'check_location')->checkbox() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="panel panel-primary">
        <div class="panel-heading"><?php echo Module::t('app', 'Contact Information') ?></div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-3">
                    <?= $form->field($model, 'contact_person')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'contact_email')->textInput(['maxlength' => true]) ?>
                </div>

                <div class="col-md-3">
                    <label for="company_phone" class="profile_label"><?= Module::t('app', 'Contact Phone') ?></label>
                    <?php if (!empty($model->contact_phone)): ?>
                        <?php foreach ($model->contact_phone as $key => $phone): ?>
                            <?php if ($key == 0): ?>
                                <?= $form->field($model, 'contact_phone[]')
                                    ->textInput([
                                        'value' => $phone
                                    ])
                                    ->label(false) ?>
                            <?php else:
                                $newAttribute = [];
                                foreach ($form->attributes as $attribute) {
                                    if ($attribute['name'] == 'contact_phone[]') {
                                        $newAttribute = $attribute;
                                    }
                                }
                                ?>
                                <div class="form-group field-companyform-contact_phone required">
                                    <?= Html::input('text', 'CompanyForm[contact_phone][]', $phone, [
                                        'class' => 'form-control',
                                        'id' => 'companyform-contact_phone_' . $key
                                    ]) ?>
                                    <div class="help-block"></div>
                                </div>
                                <?php
                                $newAttribute['id'] = 'companyform-contact_phone_' . $key;
                                $newAttribute['container'] = '.field-companyform-contact_phone_' . $key;
                                $newAttribute['input'] = '#companyform-contact_phone_' . $key;
                                array_push($form->attributes, $newAttribute);
                                ?>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <?= $form->field($model, 'contact_phone[]')
                            ->textInput()
                            ->label(false) ?>
                    <?php endif; ?>
                </div>

            </div>
        </div>
    </div>

    <hr>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading"><?php echo Module::t('app', 'Additional Information') ?></div>
                <div class="panel-body">
                    <div class="col-md-3 nopadding">
                        <?= $form->field($model, 'tailor_made')->radioList(
                            [
                                1 => Yii::t('app', 'Yes'),
                                0 => Yii::t('app', 'No')
                            ]); ?>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($model, 'bank_payment')->radioList(
                            [
                                1 => Yii::t('app', 'Yes'),
                                0 => Yii::t('app', 'No')
                            ]); ?>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($model, 'cash_payment')->radioList(
                            [
                                1 => Yii::t('app', 'Yes'),
                                0 => Yii::t('app', 'No')
                            ]); ?>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($model, 'vat_payment')->radioList(
                            [
                                1 => Yii::t('app', 'Yes'),
                                0 => Yii::t('app', 'No')
                            ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row company-characteristic">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading"><?php echo Module::t('app', 'Characteristic') ?></div>
                <div class="panel-body">
                    <div class="col-md-3 nopadding-left">
                        <div class="panel panel-primary">
                            <div class="panel-heading"><?php echo Module::t('app', 'The main specialization') ?></div>
                            <div class="panel-body">
                                <?= $form->field($model, 'main_direction')
                                    ->dropDownList(ArrayHelper::map(\backend\modules\catalogue\models\Direction::find()
                                        ->where(['lang' => $langCompany, 'status' => Status::PUBLISH])
                                        ->orderBy('name')
                                        ->all(), 'p_id', 'name'),
                                        [
                                            'prompt' => '',
                                            'data-placeholder' => Yii::t('app', 'Choose a direction'),
                                        ])
                                    ->label() ?>

                                <?php
                                $specializationList = [];
                                if ($model->main_specialization) {
                                    $direction = \backend\modules\catalogue\models\Direction::findOne(['p_id' => $model->main_direction, 'lang' => $langCompany]);
                                    $specializationList = ArrayHelper::map(\backend\modules\catalogue\models\Specialization::find()
                                        ->where([
                                            'direction_id' => $direction->id,
                                            'lang' => $langCompany,
                                            'status' => Status::PUBLISH])
                                        ->orderBy('name')
                                        ->all(), 'p_id', 'name');
                                }
                                echo $form->field($model, 'main_specialization')
                                    ->dropDownList($specializationList,
                                        [
                                            'prompt' => '',
                                            'multiple' => true,
                                            'disabled' => $model->main_specialization ? false : true,
                                            'data-placeholder' => Module::t('app', 'Choose a specialization'),
                                            'class' => 'chosen-select'
                                        ])
                                    ->label() ?>
                                <?= $form->field($model, 'check_specialization')->checkbox() ?>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-primary">
                            <div class="panel-heading"><?php echo Module::t('app', 'Additional specializations') ?></div>
                            <div class="panel-body">
                                <div class="primary_info_form_block additional_specialization fornInputBlock">
                                    <?php if (!empty($model->additional_specializations)): $i = 0; ?>
                                        <?php foreach ($model->additional_specializations as $key => $item):
                                            unset($optionDirections);
                                            $optionDirections[$key] = ["selected" => true];

                                            $optionSpecialization = [];
                                            foreach ($item as $value) {
                                                $optionSpecialization[$value] = ["selected" => true];
                                            }
                                            ?>
                                            <div class="additional_specialization_select_container company_custom_select">
                                                <?= $form->field($model, 'additional_specializations[' . $i . '][direction]')
                                                    ->dropDownList(ArrayHelper::map(\backend\modules\catalogue\models\Direction::find()
                                                        ->where(['lang' => $langCompany, 'status' => Status::PUBLISH])
                                                        ->orderBy('name')
                                                        ->all(), 'p_id', 'name'),
                                                        [
                                                            'prompt' => '',
                                                            'data-placeholder' => Module::t('app', 'Choose a direction'),
                                                            'options' => $optionDirections,
                                                            'class' => 'form-control wwdCustomSelectOnce',
                                                        ])
                                                    ->label(Module::t('app', 'Direction')) ?>

                                                <?= $form->field($model, 'additional_specializations[' . $i . '][specialization][]')
                                                    ->dropDownList(ArrayHelper::map(\backend\modules\catalogue\models\Specialization::getSpecializationDirection($key)->all(), 'p_id', 'name'),
                                                        [
                                                            'prompt' => '',
                                                            'multiple' => true,
                                                            'data-placeholder' => Module::t('app', 'Choose a specialization'),
                                                            'class' => 'chosen-select',
                                                            'options' => $optionSpecialization,
                                                        ])
                                                    ->label(Module::t('app', 'Specializations')) ?>

                                                <?= $form->field($model, 'check_additional_specialization[' . $i . ']')->checkbox(['class' => 'check_additional_specialization', 'checked' => !empty($model->check_additional_specialization[$key]) ? 'checked' : false]) ?>

                                                <?php if ($i != 0): ?>
                                                    <div class="removeAdditionalSpec">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="13" height="17"
                                                             viewBox="0 0 13 17">
                                                            <g>
                                                                <g>
                                                                    <g>
                                                                        <path fill="#FF2D2D"
                                                                              d="M11.972 3.994H1.028V3.16c0-.067.028-.105.093-.105H11.88c.065 0 .093.038.093.105zm-1.057 11.952h-8.83c-.065 0-.093-.028-.093-.095V5.057h9.016v10.794c0 .067-.028.095-.093.095zM8.645 1.053v.949h-4.3v-.949zm3.234.949H9.683V.527A.519.519 0 0 0 9.163 0c-.027 0-.046.01-.055.02A.07.07 0 0 0 9.071 0H3.836c-.287 0-.51.23-.51.527v1.475H1.121C.491 2.002 0 2.509 0 3.16v1.896h.973v10.794c0 .651.482 1.149 1.112 1.149h8.83c.63 0 1.121-.498 1.121-1.15V5.058H13V3.161C13 2.509 12.509 2 11.879 2z"/>
                                                                    </g>
                                                                    <g>
                                                                        <path fill="#FF2D2D" d="M8 6h1v9H8z"/>
                                                                    </g>
                                                                    <g>
                                                                        <path fill="#FF2D2D" d="M6 6h1v9H6z"/>
                                                                    </g>
                                                                    <g>
                                                                        <path fill="#FF2D2D" d="M4 6h1v9H4z"/>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </svg>
                                                    </div>
                                                <?php endif; ?>
                                                <hr class="hr-style3">
                                            </div>
                                            <?php $i++; ?>
                                        <?php endforeach; ?>
                                    <?php else: ?>
                                        <div class="additional_specialization_select_container company_custom_select">
                                            <?= $form->field($model, 'additional_specializations[0][direction]')
                                                ->dropDownList(ArrayHelper::map(\backend\modules\catalogue\models\Direction::find()
                                                    ->where(['lang' => $langCompany, 'status' => Status::PUBLISH])
                                                    ->orderBy('name')
                                                    ->all(), 'p_id', 'name'),
                                                    [
                                                        'prompt' => '',
                                                        'data-placeholder' => Module::t('app', 'Choose a direction'),
                                                        'class' => 'form-control wwdCustomSelectOnce',
                                                    ])
                                                ->label(Module::t('app', 'Direction')) ?>

                                            <?= $form->field($model, 'additional_specializations[0][specialization][]')
                                                ->dropDownList(ArrayHelper::map(\backend\modules\catalogue\models\Direction::find()
                                                    ->where(['lang' => $langCompany, 'status' => Status::PUBLISH])
                                                    ->orderBy('name')
                                                    ->all(), 'p_id', 'name'),
                                                    [
                                                        'prompt' => '',
                                                        'multiple' => true,
                                                        'data-placeholder' => Module::t('app', 'Choose a specialization'),
                                                        'class' => 'chosen-select',
                                                    ])
                                                ->label(Module::t('app', 'Specializations')) ?>

                                            <?= $form->field($model, 'check_additional_specialization[0]')->checkbox(['class' => 'check_additional_specialization']) ?>

                                        </div>
                                    <?php endif; ?>
                                </div>
                                <div class="add_specialization_block">
                                    <a href="#!"
                                       class="add_specialization"><?= Module::t('app', 'Add specialization') ?></a>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="panel panel-primary">
                            <div class="panel-heading"><?php echo Module::t('app', 'Equipment') ?></div>
                            <div class="panel-body">
                                <?php
                                foreach ($equipments as $key => $equipment): ?>
                                    <div class="col-md-6 nopadding-left">
                                        <div class="form-check">
                                            <input <?= key_exists($equipment->p_id, $model->equipment) ? 'checked' : '' ?>
                                                    name="<?= $model->formName() ?>[equipment][<?= $equipment->p_id ?>]"
                                                    class="form-check-input position-static" type="checkbox"
                                                    id="equipment_<?= $equipment->p_id ?>"
                                                    value="<?= $equipment->p_id ?>">
                                            <label class="form-check-label"
                                                   for="equipment_<?= $equipment->p_id ?>"><?= $equipment->name ?></label>
                                        </div>
                                        <?= $form->field($model, 'check_equipment[' . $equipment->p_id . ']')
                                            ->checkbox(['class' => 'check_equipment', 'checked' => !empty($model->equipment[$equipment->p_id]) ? 'checked' : false]) ?>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 nopadding-left">
                        <div class="panel panel-primary">
                            <div class="panel-heading"><?php echo Module::t('app', 'Photo production') ?></div>
                            <div class="panel-body primary_info_form_block" id="company_photos">
                                <?= $form->field($model, 'photos',
                                    [
                                        'options' => [
                                            'id' => 'logo_photos',
                                        ]
                                    ])
                                    ->hiddenInput()
                                    ->label(false) ?>
                                <?php
                                if (!empty($model->photos)):
                                    $photos = \yii\helpers\Json::decode($model->photos);
                                    foreach ($photos as $item):
                                        $img = (!empty($item["crop_2"])) ? $item["crop_2"] : $item["origin"]; ?>
                                        <div class="profile_input_file">
                                            <img data-origin="<?= $item["origin"] ?>" src="<?= $img ?>">
                                            <a href="#" class="delete">+</a>
                                        </div>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                                <div class="profile_input_file add"
                                     style="background-image: url('/img/icons/add_photo_icon.svg');">
                                    <input type="file" id="photos" name="photos" class="formInput" multiple>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="panel panel-primary">
                            <div class="panel-heading"><?php echo Module::t('app', 'Company logo') ?></div>
                            <div class="panel-body">
                                <?= $form->field($model, 'logo',
                                    [
                                        'options' => [
                                            'id' => 'logo_img',
                                        ]
                                    ])
                                    ->hiddenInput()
                                    ->label(false) ?>
                                <div class="profile_input_file" id="logo_container"
                                     style="">
                                    <?php if (!empty($model->logo)): ?>
                                        <img src="<?= $model->logo ?>"><a href="#" class="delete">+</a>
                                    <?php else: ?>
                                        <input type="file" id="logo" name="logo" class="formInput">
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="col-md-6 nopadding">
                <?= Html::submitButton(Yii::t('app', 'Save'), ['name' => $model->formName() . '[status]', 'value' => 200, 'class' => 'btn btn-primary']) ?>
                <?php $companyUser = \common\models\User::findOne(['id' => $model->user_id, 'status' => \common\models\User::STATUS_ACTIVE]);
                if (!empty($companyUser)): ?>
                    <?= Html::submitButton(Module::t('app', 'Save and add to directory'), ['name' => $model->formName() . '[status]', 'value' => 100, 'class' => 'btn btn-success']) ?>
                <?php endif; ?>
                <?= Html::submitButton(Yii::t('app', 'Delete'), ['name' => $model->formName() . '[status]', 'value' => 500, 'class' => 'btn btn-danger']) ?>
                <?php if (!empty($model->id)): ?>
                    <?= Html::button(Module::t('app', 'Reset Password'), ['class' => 'btn btn-danger sendMailButton', 'data-company-id' => $model->id]) ?>
                <?php endif; ?>
            </div>
            <div class="col-md-6 nopadding">
                <div class="edit-url-block" data-id="<?= $model->id ?>">
                    <label><?= Module::t('app', 'Profile link') ?>:</label>
                    <span><?= Yii::$app->params['basePath'] ?>/company/<?= $model->slug ?></span>
                    <input type="hidden" id="copy-url" disabled
                           value="<?= Yii::$app->params['basePath'] ?>/company/<?= $model->slug ?>">
                    <span data-toggle="tooltip" data-animation="false"
                            title="<?= Yii::t('app', 'Copy url') ?>"
                            data-title="<?= Yii::t('app', 'Copy url') ?>"
                            data-copied="<?= Yii::t('app', 'Copied') ?>"
                            data-failed="<?= Yii::t('app', 'Error copying') ?>"
                            class="fa fa-copy js-copy-url"
                            data-clipboard-text="<?= Yii::$app->params['basePath'] ?>/company/<?= $model->slug ?>"></span>
                </div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
