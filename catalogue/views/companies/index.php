<?php

use common\models\User;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Pjax;
use \backend\modules\catalogue\Module;
use \backend\modules\catalogue\enums\StatusCompany;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Module::t('app', 'List of Companies');
$this->params['breadcrumbs'][] = ['label' => Module::t('app', 'Catalogue'), 'url' => ['/catalogue', 'lang' => $data->lang]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="page-wrapper" class="list-companies">
    <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <?php try {
                echo \yii\widgets\Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]);
            } catch (Exception $e) {
            } ?>
        </div>
        <div class="row">
            <?= $this->render('_form_edit', [
                'model' => $model,
                'modelSearch' => $modelSearch
            ]) ?>
        </div>
        <div class="row">
            <?php Pjax::begin(); ?>

            <div class="panel panel-default">
                <div class="block-title panel-heading">
                    <div class="col-md-5">
                        <?= $this->title; ?>
                    </div>
                </div>

                <div class="panel-body nopadding-top">
                    <?php
                    $columns = [
                        ['class' => 'yii\grid\SerialColumn'],
                        'id',
                        [
                            'attribute' => 'name',
                            'value' => function ($model) {
                                return Html::a($model->info->name, Url::to(['update', 'id' => $model->id]), ['data-pjax' => 0]);
                            },
                            'format' => 'raw',
                        ],
                        [
                            'attribute' => 'email',
                            'value' => function ($model) {
                                return Html::a($model->email, Url::to(['update', 'id' => $model->id]), ['data-pjax' => 0]);
                            },
                            'format' => 'raw',
                        ],
                        [
                            'attribute' => 'contact_phone',
                            'headerOptions' => ['style' => 'width:155px'],
                            'value' => function ($model) {
                                $phone = \yii\helpers\Json::decode($model->contact_phone);

                                return implode($phone, '<br/>');
                            },
                            'format' => 'raw',
                        ],
                        [
                            'attribute' => 'verified',
                            'label' => Module::t('app', 'Verified'),
                            'value' => function ($model) {
                                if ($model->verified) {
                                    return '<i class="fa fa-circle" style="color: green" title="' . Module::t('app', 'Yes') . '"></i>';
                                } else {
                                    return '<i class="fa fa-circle" style="color: red" title="' . Module::t('app', 'No') . '"></i>';
                                }
                            },
                            'format' => 'raw'
                        ],
                        [
                            'attribute' => 'associated',
                            'label' => Module::t('app', 'Associated'),
                            'value' => function ($model) {
                                if ($model->associated) {
                                    return '<i class="fa fa-circle" style="color: green" title="' . Module::t('app', 'Yes') . '"></i>';
                                } else {
                                    return '<i class="fa fa-circle" style="color: red" title="' . Module::t('app', 'No') . '"></i>';
                                }
                            },
                            'format' => 'raw'
                        ],
                        'contact_email',
                        [
                            'attribute' => 'user_id',
                            'value' => function ($model) {
                                if ($model->user_id) {
                                    $user = User::findOne($model->user_id);
                                    if ($user) {
                                        $name = $user->firstname . ' ' . $user->lastname;
                                        return Html::a(Html::encode($name), Url::toRoute(['/authusers/view', 'id' => $user->id]), ['data-pjax' => 0]);
                                    } else return Yii::t('app', 'User deleted') . ' (id ' . $model->user_id . ')';
                                }
                                return Yii::$app->user->getisGuest();
                            },
                            'format' => 'raw',
                        ],
                        [
                            'attribute' => 'user_verified',
                            'value' => function ($model) {
                                if ($model->user_verified) {
                                    $user = User::findOne($model->user_verified);
                                    if ($user) {
                                        $name = $user->firstname . ' ' . $user->lastname;
                                        return Html::encode($name);
                                    } else return Yii::t('app', 'User deleted') . ' (id ' . $model->user_id . ')';
                                }
                                return Yii::$app->user->getisGuest();
                            },
                            'format' => 'raw',
                            'label' => Module::t('app', 'User Verified'),

                        ],
                        [
                            'attribute' => 'created_at',
                            'value' => function ($model) {
                                return Yii::$app->formatter->asDatetime($model->created_at, 'medium');
                            },
                            'format' => 'raw',
                        ],
                        [
                            'attribute' => 'updated_at',
                            'value' => function ($model) {
                                return Yii::$app->formatter->asDatetime($model->updated_at, 'medium');
                            },
                            'format' => 'raw',
                        ],
                        [
                            'attribute' => 'status',
                            'value' => function ($model) {
                                $status = StatusCompany::getStatusAdmin($model->status);
                                return $status;
                            },
                            'label' => Yii::t('app', 'Status'),
                            'format' => 'raw',
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            /*'buttons' => [
                                'view' => function ($url, $model) {
                                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', Url::to(['view', 'id' => $model->p_id, 'lang' => $model->lang]), ['data-pjax' => 0]);
                                },
                                'update' => function ($url, $model) {
                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Url::to(['update', 'id' => $model->p_id, 'lang' => $model->lang]), ['data-pjax' => 0]);
                                },
                            ],*/
                        ],
                    ];
                    ?>
                    <?php
                    echo GridView::widget([
                        'dataProvider' => $dataProvider,
                        'layout' => '{items}<div class="col-sm-4 nopadding">{summary}</div>
                            <div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate">{pager}</div>',
                        'columns' => $columns,
                    ]); ?>
                </div>

            </div>

            <?php Pjax::end(); ?>

        </div>
    </div>
</div>
