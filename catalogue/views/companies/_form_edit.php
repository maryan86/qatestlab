<?php

use backend\modules\catalogue\enums\StatusCompany;
use backend\modules\catalogue\Module;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \backend\modules\catalogue\models\Region;
use yii\helpers\Url;
use backend\modules\catalogue\assets\CatalogueAssets;

CatalogueAssets::register($this);
?>
<div class="panel panel-default">
    <div class="block-title panel-heading">
        <div class="col-md-5">
            <?= Yii::t('app', 'Search') ?>
        </div>
    </div>
    <div class="panel-body nopadding-top">
        <div class="col-md-12 nopadding">
            <?php $form = ActiveForm::begin(['action' => Url::toRoute(['/catalogue/companies']),'method' => 'get', 'id' => "companies_search"]); ?>
            <div class="row">
                <div class="col-md-2">
                    <?= $form->field($modelSearch, 'name', ['inputOptions' => ['placeholder' => Module::t('app', 'Company Name'), 'class' => 'form-control']])->textInput()->label(false) ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($modelSearch, 'email', ['inputOptions' => ['placeholder' => Module::t('app', 'Company E-mail'), 'class' => 'form-control']])->textInput()->label(false) ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($modelSearch, 'contact_phone', ['inputOptions' => ['placeholder' => Module::t('app', 'Contact Phone'), 'class' => 'form-control']])->textInput()->label(false) ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($modelSearch, 'contact_email', ['inputOptions' => ['placeholder' => Module::t('app', 'Contact E-mail'), 'class' => 'form-control']])->textInput()->label(false) ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($modelSearch, 'user', ['inputOptions' => ['placeholder' => Yii::t('app', 'User'), 'class' => 'form-control']])->textInput()->label(false) ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($modelSearch, 'edrpou', ['inputOptions' => ['placeholder' => Module::t('app', 'Edrpou code'), 'class' => 'form-control']])->textInput()->label(false) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <?= $form->field($modelSearch, 'verified')->checkbox() ?>
                    <?= $form->field($modelSearch, 'associated')->checkbox() ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($modelSearch, 'date',
                        ['inputOptions' => ['placeholder' => Module::t('app', 'Date'), 'class' => 'form-control', 'id' => 'datepicker'],
                            'options' => ['class' => 'fa fa-calendar calendar-icon']])
                        ->input('date')->label(false) ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($modelSearch, 'region',
                        ['options' => [
                            'class' => 'form-group'
                        ]])
                        ->dropDownList(Region::getAll(),
                            ['prompt' => Module::t('app', 'Region')])->label(false)
                    ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($modelSearch, 'status',
                        ['options' => [
                            'class' => 'form-group'
                        ]])
                        ->dropDownList(StatusCompany::renderStatus(),
                            ['prompt' => Yii::t('app', 'Status')])->label(false)
                    ?>
                </div>
                <div class="col-md-4">
                    <?= Html::a(Yii::t('app', 'Clear'), Url::toRoute(['/catalogue/companies']), ['class' => 'button gray resetFilter']) ?>
                    <?= Html::submitButton(Yii::t('app', 'Apply filter'), ['class' => 'button blue', 'data-pjax' => true]) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
