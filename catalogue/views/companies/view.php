<?php

use yii\helpers\Html;
use yii\helpers\Json;
use yii\widgets\DetailView;
use \backend\modules\catalogue\Module;
use \backend\modules\catalogue\enums\StatusCompany;
use \backend\modules\multilang\models\Lang;
use backend\modules\catalogue\assets\CatalogueAssets;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\helpers\HtmlPurifier;

/* @var $this yii\web\View */
/* @var $model backend\modules\catalogue\models\Direction */

$this->title = $model->info->name;
$this->params['breadcrumbs'][] = ['label' => Module::t('app', 'Catalogue'), 'url' => ['/catalogue', 'lang' => $data->lang]];
$this->params['breadcrumbs'][] = ['label' => Module::t('app', 'List of Companies'), 'url' => ['index', 'lang' => $data->lang]];
$this->params['breadcrumbs'][] = $this->title;

CatalogueAssets::register($this);
?>

<div id="page-wrapper">
    <div class="row company-view">
        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <?php echo \yii\widgets\Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]); ?>

            <div class="panel panel-info">
                <div class="block-title panel-heading">
                    <div class="col-md-10">
                        <?= Html::encode($this->title) ?>
                    </div>
                </div>
                <div class="panel-body nopadding-top">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            [
                                'value' => function ($model) {
                                    if (!empty($model->infoLanguages['ua']->name)) {
                                        return $model->infoLanguages['ua']->name;
                                    }
                                    return '-';
                                },
                                'label' => Module::t('app', 'Company Name') . ' [' . Lang::find()->where(['url' => 'ua'])->one()->name . ']'
                            ],
                            [
                                'value' => function ($model) {
                                    if (!empty($model->infoLanguages['en']->name)) {
                                        return $model->infoLanguages['en']->name;
                                    }
                                    return '-';
                                },
                                'label' => Module::t('app', 'Company Name') . ' [' . Lang::find()->where(['url' => 'en'])->one()->name . ']'
                            ],

                            [
                                'attribute' => 'verified',
                                'label' => Module::t('app', 'Verified'),
                                'value' => function ($model) {
                                    if ($model->verified) {
                                        return '<i class="fa fa-circle" style="color: green" title="' . Module::t('app', 'Yes') . '"></i>';
                                    } else {
                                        return '<i class="fa fa-circle" style="color: red" title="' . Module::t('app', 'No') . '"></i>';
                                    }
                                },
                                'format' => 'raw'
                            ],
                            [
                                'attribute' => 'associated',
                                'label' => Module::t('app', 'Associated'),
                                'value' => function ($model) {
                                    if ($model->associated) {
                                        return '<i class="fa fa-circle" style="color: green" title="' . Module::t('app', 'Yes') . '"></i>';
                                    } else {
                                        return '<i class="fa fa-circle" style="color: red" title="' . Module::t('app', 'No') . '"></i>';
                                    }
                                },
                                'format' => 'raw'
                            ],
                            'email',
                            'edrpou',
                            'site',
                            'year',
                            'personnel_quantity',
                            [
                                'value' => function ($model) {
                                    return $model->capacity_min . ' / ' . $model->capacity_max . ' ' . Yii::$app->params['capacityType'][$model->info->lang][$model->capacity_type];
                                },
                                'label' => Module::t('app', 'Minimum and maximum order')
                            ],
                            [
                                'attribute' => 'city',
                                'value' => function ($model) {
                                    if (!empty($model->info->city) || !empty($model->region->name)) {
                                        return $model->info->city . ', ' . $model->region->name;
                                    }
                                    return '-';
                                },
                            ],

                            [
                                'value' => function ($model) {
                                    if (!empty($model->infoLanguages['ua']->description)) {
                                        return $model->infoLanguages['ua']->description;
                                    }
                                    return '-';
                                },
                                'label' => Module::t('app', 'A brief description of the company') . ' [' . Lang::find()->where(['url' => 'ua'])->one()->name . ']'
                            ],
                            [
                                'value' => function ($model) {
                                    if (!empty($model->infoLanguages['en']->description)) {
                                        return $model->infoLanguages['en']->description;
                                    }
                                    return '-';
                                },
                                'label' => Module::t('app', 'A brief description of the company') . ' [' . Lang::find()->where(['url' => 'en'])->one()->name . ']'
                            ],

                            'contact_person',
                            'contact_email',
                            [
                                'attribute' => 'contact_phone',
                                'value' => function ($model) {
                                    $phones = Json::decode($model->contact_phone);
                                    return implode($phones, '<br/>');
                                },
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'tailor_made',
                                'value' => function ($model) {
                                    if ($model->tailor_made) {
                                        return '<i class="fa fa-circle" style="color: green" title="' . Module::t('app', 'Yes') . '"></i>';
                                    } else {
                                        return '<i class="fa fa-circle" style="color: red" title="' . Module::t('app', 'No') . '"></i>';
                                    }
                                },
                                'format' => 'raw'
                            ],
                            [
                                'attribute' => 'bank_payment',
                                'value' => function ($model) {
                                    if ($model->bank_payment) {
                                        return '<i class="fa fa-circle" style="color: green" title="' . Module::t('app', 'Yes') . '"></i>';
                                    } else {
                                        return '<i class="fa fa-circle" style="color: red" title="' . Module::t('app', 'No') . '"></i>';
                                    }
                                },
                                'format' => 'raw'
                            ],
                            [
                                'attribute' => 'cash_payment',
                                'value' => function ($model) {
                                    if ($model->cash_payment) {
                                        return '<i class="fa fa-circle" style="color: green" title="' . Module::t('app', 'Yes') . '"></i>';
                                    } else {
                                        return '<i class="fa fa-circle" style="color: red" title="' . Module::t('app', 'No') . '"></i>';
                                    }
                                },
                                'format' => 'raw'
                            ],
                            [
                                'attribute' => 'vat_payment',
                                'value' => function ($model) {
                                    if ($model->vat_payment) {
                                        return '<i class="fa fa-circle" style="color: green" title="' . Module::t('app', 'Yes') . '"></i>';
                                    } else {
                                        return '<i class="fa fa-circle" style="color: red" title="' . Module::t('app', 'No') . '"></i>';
                                    }
                                },
                                'format' => 'raw'
                            ],
                            [
                                'value' => function ($model) {
                                    $specializations = $model->getSpecializationSpecialOrder();
                                    $result = '';
                                    foreach ($specializations as $specialization) {
                                        $result .= '- ' . $specialization->specialization->name;
                                        $result .= $specialization->main_specialization ? ' (' . Module::t('app', 'The main specialization') . ')' : ' (' . Module::t('app', 'Additional specialization') . ')';
                                        $result .= "<br />";
                                    }
                                    return $result;
                                },
                                'label' => Module::t('app', 'Specializations'),
                                'format' => 'raw'
                            ],

                            [
                                'value' => function ($model) {
                                    $equipments = $model->getEquipmentSpecialOrder();
                                    $result = '';
                                    foreach ($equipments as $equipment) {
                                        $result .= '- ' . $equipment->equipment->name;
                                        $result .= "<br />";
                                    }
                                    return $result;
                                },
                                'label' => Module::t('app', 'Equipment'),
                                'format' => 'raw'
                            ],
                            [
                                'attribute' => 'logo',
                                'value' => function ($model) {
                                    if ($model->logo) {
                                        return Html::img($model->logo, ['width' => 150]);
                                    } else {
                                        return Module::t('app', 'Not downloaded');
                                    }
                                },
                                'format' => 'raw'
                            ],
                            [
                                'attribute' => 'photos',
                                'value' => function ($model) {
                                    if ($model->photos) {
                                        $photos = \yii\helpers\Json::decode($model->photos);
                                        $resPhoto = '';
                                        foreach ($photos as $item):
                                            $img = (!empty($item["crop_2"])) ? $item["crop_2"] : $item["origin"];
                                            $resPhoto .= '<div class="profile_input_file">
                                            <img data-origin="' . $item["origin"] . '" src="' . $img . '">
                                        </div>';
                                        endforeach;
                                        return $resPhoto;
                                        return Html::img($model->logo, ['width' => 150]);
                                    } else {
                                        return Module::t('app', 'Not downloaded');
                                    }
                                },
                                'format' => 'raw'
                            ],
                            [
                                'attribute' => 'user_id',
                                'value' => function ($model) {
                                    if ($model->user_id) {
                                        $user = \common\models\User::findOne($model->user_id);
                                        if ($user) {
                                            $name = $user->firstname . ' ' . $user->lastname;
                                            return Html::a(Html::encode($name), \yii\helpers\Url::toRoute(['/authusers/view', 'id' => $user->id]), ['data-pjax' => 0]);
                                        } else return Yii::t('app', 'User deleted') . ' (id ' . $model->user_id . ')';
                                    }
                                    return Yii::$app->user->getisGuest();
                                },
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'created_at',
                                'value' => function ($model) {
                                    return Yii::$app->formatter->asDatetime($model->created_at, 'full');
                                },
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'updated_at',
                                'value' => function ($model) {
                                    return Yii::$app->formatter->asDatetime($model->updated_at, 'full');
                                },
                            ],
                            [
                                'attribute' => 'date_verified',
                                'value' => function ($model) {
                                    return Yii::$app->formatter->asDatetime($model->date_verified, 'full');
                                },
                            ],

                            [
                                'attribute' => 'status',
                                'value' => function ($model) {
                                    return StatusCompany::getStatusAdmin($model->status);
                                },
                                'label' => Yii::t('app', 'Status'),
                                'format' => 'raw',
                            ],
                            [
                                'value' => function ($model) {
                                    return Html::a(Yii::$app->params['basePath'] . '/company/' . $model->slug, Yii::$app->params['basePath'] . '/company/' . $model->slug, ['target' => '_blank']);
                                },
                                'format' => 'raw',
                                'label' => Module::t('app', 'Profile link')
                            ]
                        ],
                    ]) ?>
                </div>
            </div>
            <?php if ($reviews->totalCount > 0) : ?>
                <div class="panel panel-info">
                    <div class="panel-heading"><a
                                href="<?= Url::to(['/catalogue/reviews', 'user_id' => $model->user_id]) ?>"><?php echo Module::t('app', 'Company Review') ?></a>
                    </div>
                    <div class="panel-body">
                        <?php
                        echo GridView::widget([
                            'dataProvider' => $reviews,
                            'layout' => '{items}<div class="col-sm-4 nopadding">{summary}</div>
                            <div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate">{pager}</div>',
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                'id',

                                [
                                    'attribute' => 'user_id',
                                    'value' => function ($model) {
                                        if ($model->user) {
                                            $user = $model->user;
                                            if ($user) {
                                                $name = $user->firstname . ' ' . $user->lastname;
                                                return Html::a(Html::encode($name), Url::toRoute(['/authusers/view', 'id' => $user->id]), ['data-pjax' => 0]);
                                            } else return Yii::t('app', 'User deleted') . ' (id ' . $model->user_id . ')';
                                        }
                                        return Yii::$app->user->getisGuest();
                                    },
                                    'format' => 'raw',
                                ],
                                [
                                    'attribute' => 'review',
                                    'value' => function ($model) {
                                        $text = HtmlPurifier::process(strip_tags($model->review));
                                        $text = \common\models\Functions::cutStringSpace($text, 100);
                                        return Html::a($text, Url::toRoute(['/catalogue/reviews/view', 'id' => $model->id]), ['data-pjax' => 0]);
                                    },
                                    'format' => 'raw',
                                ],
                                [
                                    'attribute' => 'created_at',
                                    'value' => function ($model) {
                                        return Yii::$app->formatter->asDatetime($model->created_at, 'medium');
                                    },
                                    'format' => 'raw',
                                ],

                                [
                                    'attribute' => 'status',
                                    'value' => function ($model) {
                                        $status = \backend\modules\catalogue\enums\StatusReviews::getStatusAdmin($model->status);
                                        if ($model->status == \backend\modules\catalogue\enums\StatusReviews::TRASH) {
                                            $status .= Html::a('<span class="fa fa-undo" aria-hidden="true"></span>', Url::toRoute(['restore', 'id' => $model->id]), ['data-pjax' => 0, 'data-confirm' => Module::t('app', 'You are sure you want to recover the data?'), 'data-method' => 'post', 'class' => 'restore']);
                                        }
                                        return $status;
                                    },
                                    'label' => Yii::t('app', 'Status'),
                                    'format' => 'raw',
                                ],
                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'buttons' => [
                                        'update' => function () {
                                            return false;
                                        },
                                        'delete' => function () {
                                            return false;
                                        },
                                        'view' => function ($url, $model) {
                                            $url = Url::toRoute(['/catalogue/reviews/view', 'id' => $model->id]);
                                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                                'title' => Yii::t('yii', 'View'),
                                                'data-pjax' => 0
                                            ]);
                                        },
                                    ],
                                ],
                            ],
                        ]); ?>
                    </div>
                </div>
            <?php endif; ?>
            <div class="panel panel-info">
                <div class="block-title panel-heading">
                </div>
                <div class="panel-body nopadding-top">
                    <div class="row col-md-12">
                        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                'method' => 'post',
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
