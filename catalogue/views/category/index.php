<?php

use backend\modules\catalogue\assets\CatalogueAssets;
use backend\modules\catalogue\models\CategoryFilter;
use common\models\Functions;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Pjax;
use \backend\modules\catalogue\Module;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
CatalogueAssets::register($this);
$this->title = Module::t('app', 'Category Specialization');
$this->params['breadcrumbs'][] = ['label' => Module::t('app', 'Catalogue'), 'url' => ['/catalogue', 'lang' => $data->lang]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <?php echo \yii\widgets\Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]); ?>

            <?php Pjax::begin(); ?>

            <div class="panel panel-default">
                <div class="block-title panel-heading">
                    <div class="col-md-4">
                        <?= $this->title; ?>
                    </div>
                    <div class="col-md-4">
                        <a href="<?= Url::to(['create', 'lang' => $data->lang]) ?>" data-pjax="0" class="button blue">
                            <span class="glyphicon plus glyphicon-plus"></span><?= Yii::t('app', 'Add') ?>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <?php echo $this->render("/catalogue/langLayouts", ['data' => $data]) ?>
                    </div>
                </div>

                <div class="panel-body nopadding-top">
                    <?php
                    $columns = [
                        ['class' => 'yii\grid\SerialColumn'],
                        'id',
                        [
                            'attribute' => 'name',
                            'value' => function ($model) {
                                return Html::a($model->name, Url::to(['update', 'id' => $model->p_id, 'lang' => $model->lang]), ['data-pjax' => 0]);
                            },
                            'format' => 'raw',
                        ],
                        [
                            'attribute' => 'direction.name',
                            'label' => Module::t('app', 'Direction'),
                            'format' => 'raw',
                        ],
                        [
                            'label' => Module::t('app', 'Filter'),
                            'value' => function ($model) {
                                $categoryFilter = CategoryFilter::find()->where(['catalogue_category_id' => $model->id])->all();
                                $return = '';
                                foreach ($categoryFilter as $item) {
                                    $return .= '- ' . $item->name . '<br>';
                                }
                                return $return ? $return : Module::t('app', 'Not shared');
                            },
                            'format' => 'raw',
                        ],
                        [
                            'attribute' => 'status',
                            'value' => function ($model) {
                                $status = Functions::getStatusText($model->status);
                                return $status;
                            },
                            'label' => Yii::t('app', 'Status'),
                            'format' => 'raw',
                        ],
                        [
                            'attribute' => 'created_at',
                            'value' => function ($model) {
                                return Yii::$app->formatter->asDatetime($model->created_at, 'medium');
                            },
                            'format' => 'raw',
                        ],
                        [
                            'attribute' => 'updated_at',
                            'value' => function ($model) {
                                return Yii::$app->formatter->asDatetime($model->updated_at, 'medium');
                            },
                            'format' => 'raw',
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'buttons' => [
                                'view' => function ($url, $model) {
                                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', Url::to(['view', 'id' => $model->p_id, 'lang' => $model->lang]), ['data-pjax' => 0]);
                                },
                                'update' => function ($url, $model) {
                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Url::to(['update', 'id' => $model->p_id, 'lang' => $model->lang]), ['data-pjax' => 0]);
                                },
                            ],
                        ],
                    ];
                    ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'layout' => '{items}<div class="col-sm-4 nopadding">{summary}</div>
                        <div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate">{pager}</div>',
                        'columns' => \common\models\base\CmsModel::getGridColumn($columns, $data, $model, $otherLangModels),
                    ]); ?>
                </div>

            </div>

            <?php Pjax::end(); ?>

        </div>
    </div>
</div>
