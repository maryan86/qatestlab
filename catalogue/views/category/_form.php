<?php

use backend\modules\catalogue\models\Direction;
use backend\modules\catalogue\Module;
use backend\modules\catalogue\enums\Status;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\catalogue\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="direction-form">

    <div class="col-md-12 nopadding">

        <?php $form = ActiveForm::begin(); ?>

        <div class="col-md-6 nopadding">

            <?= $form->field($model, 'lang')->hiddenInput(['maxlength' => true, 'value' => $data->lang])->label(false) ?>

            <?= $form->field($model, 'direction_id',
                ['options' => [
                    'class' => 'form-group'
                ]])
                ->dropDownList(ArrayHelper::map(Direction::find()
                    ->where(['lang' => $data->lang, 'status' => Status::PUBLISH])
                    ->orderBy('name')
                    ->all(), 'id', 'name'),
                    ['prompt' => '']) ?>

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <div class="form-group field-filter-name dedicated__block">
                <label><?php echo Module::t('app', 'Filter') ?></label>

                <?php if (!empty($categoryFilter)):
                    foreach ($categoryFilter as $item):
                        ?>
                        <div class="form-group field-category-filter">
                            <input type="text" class="form-control category-filter display-inline"
                                   value="<?= $item->name ?>" name="Category[filter][id][<?= $item->id ?>]">
                            <a class="btn btn-danger display-inline remove_filter"
                               title="<?= Yii::t('app', 'Delete') ?>"><span
                                        class="glyphicon -minus glyphicon-minus"></span></a>
                        </div>
                    <?php endforeach;
                endif; ?>

                <button class="btn btn-warning" id="add_filter">
                    <span class="glyphicon plus glyphicon-plus"></span><?php echo Yii::t('app', 'Add') ?>
                </button>
            </div>

        </div>

        <div class="nopadding col-md-12">
            <div class="col-md-6 settings-form">
                <?php $model->status = $model->isNewRecord ? 100 : $model->status; ?>
                <?= $form->field($model, 'status',
                    ['options' => [
                        'class' => 'select-status col-md-3 nopadding-left'
                    ]])
                    ->dropDownList(
                        Status::renderStatus(),
                        [
                            'prompt' => '',
                            'options' =>
                                [
                                    500 => ['disabled' => $model->isNewRecord ? true : false]
                                ]
                        ]
                    );
                ?>

                <div class="form-group button-group">
                    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'button fa fa-floppy-o blue']) ?>
                </div>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
