<?php
use backend\modules\menu\MenuModule;
use \yii\helpers\Html;
if(empty($menuId)) {
    $this->registerCss(".menu-parent { display: none; }");
    $checked = false;
    $selected = false;
    $value = 0;
}else {
    $this->registerCss(".menu-parent { display: block; }");
    $checked = true;
    $selected = $menuId->parent_id ?? false;
    $value = 1;
}

?>
<div class="col-md-12 custom-height">
    <div class="row">
        <div class="col-md-12">
            <label><?= MenuModule::t('app', 'Add page to menu')?></label>
        </div>
        <div class="col-md-2">
            <?= Html::checkbox('Menu[check_menu]', $checked,
                [
                    'class' => 'swicher js-switcer default',
                    'onchange' =>'         
                        if ($(this).prop("checked")) {
                            $(".menu-parent").show();
                            $(this).val(1);
                        }else{
                            $(this).val(0);
                            $(".menu-parent").hide();
                        }',
                    'value' => $value,
                ]
            ); ?>
        </div>
        <div class="col-md-6 menu-parent">
            <?= Html::dropDownList('Menu[parent_id]', $selected, $menu, ['data-placeholder' => MenuModule::t('app', 'Select a parent item'),
                'class' => 'chosen-select menu-parent-element',
                'prompt' => MenuModule::t('app', 'Select a parent item')]) ?>
        </div>
    </div>
</div>