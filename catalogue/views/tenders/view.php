<?php

use backend\modules\catalogue\models\CompanyTenders;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;
use \backend\modules\catalogue\Module;

/* @var $this yii\web\View */
/* @var $model backend\modules\catalogue\models\Direction */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Module::t('app', 'Catalogue'), 'url' => ['/catalogue', 'lang' => $data->lang]];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['index', 'lang' => $data->lang]];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

?>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <?php echo \yii\widgets\Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]); ?>

            <div class="panel panel-default">
                <div class="block-title panel-heading">
                    <div class="col-md-9">
                        <?= Html::encode($this->title) ?>
                    </div>
                </div>
                <div class="panel-body nopadding-top">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            'name',
                            [
                                'attribute' => 'direction',
                                'value' => function ($model) use ($data) {
                                    $direction = CompanyTenders::getDirectionItem($model->direction->tenders_direction_pid, $data->lang);
                                    return $direction->name;
                                },
                                'label' => Module::t('app', 'Production direction'),
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'specialization',
                                'value' => function ($model) use ($data) {
                                    $specializationIDs = ArrayHelper::getColumn($model->specialization, 'tenders_specialization_pid');
                                    $specializations = CompanyTenders::getSpecializationItems($specializationIDs, $data->lang);
                                    return implode(', ', ArrayHelper::getColumn($specializations, 'name'));
                                },
                                'label' => Module::t('app', 'Production specialization'),
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'region',
                                'value' => function ($model) use ($data) {
                                    return $model->region->name;
                                },
                                'label' => Module::t('app', 'Region'),
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'quantity',
                                'value' => function ($model) use ($data) {
                                    $quantity = $model->quantity_min == $model->quantity_max ? Yii::$app->formatter->asDecimal($model->quantity_min, 0) : Yii::$app->formatter->asDecimal($model->quantity_min, 0) . ' - ' . Yii::$app->formatter->asDecimal($model->quantity_max, 0);
                                    return $quantity . '&nbsp;' . Yii::$app->params['capacityType'][$data->lang][$model->quantity_type];
                                },
                                'label' => Module::t('app', 'Quantity'),
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'purchase_amount',
                                'value' => function ($model) use ($data) {
                                    return Yii::$app->formatter->asDecimal($model->purchase_amount, 0) . '&nbsp;' . Yii::$app->params['purchaseCurrency'][$data->lang][$model->purchase_currency];
                                },
                                'label' => Module::t('app', 'Purchase amount'),
                                'format' => 'raw',
                            ],
                            'description',
                            [
                                'attribute' => 'attachment',
                                'value' => function ($model) {
                                    $html = '';
                                    if ($model->attachment) {
                                        $attchments = \yii\helpers\Json::decode($model->attachment, false);

                                        foreach ($attchments as $attchment) {
                                            $html .= '<div class="tenders_input_file"><a data-pjax="0" class="attachment" href="' . $attchment->src . '" target="_blank">' . $attchment->name . '</a></div>';
                                        }
                                    }
                                    return $html;
                                },
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'proposal_to',
                                'value' => function ($model) {
                                    return Yii::$app->formatter->asDate($model->proposal_to, 'php:d.m.Y');
                                },
                                'format' => 'raw',
                            ],
                            'name_customer',
                            'address_customer',
                            'contact_person',
                            'contact_phone',
                            'contact_email',
                            [
                                'attribute' => 'created_at',
                                'value' => function ($model) {
                                    return Yii::$app->formatter->asDatetime($model->created_at, 'full');
                                },
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'updated_at',
                                'value' => function ($model) {
                                    return Yii::$app->formatter->asDatetime($model->updated_at, 'full');
                                },
                                'format' => 'raw',
                            ],

                        ],
                    ]) ?>

                    <div class="row col-md-12">
                        <?= Html::a('<i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;' . Yii::t('app', 'Go to back'), ['catalogue/tenders'], ['class' => 'btn btn-primary']) ?>
                        <?php if ($model->status == \backend\modules\catalogue\enums\StatusTenders::TRASH) : ?>
                            <?= Html::a('<i class="fa fa-undo" aria-hidden="true"></i>&nbsp;' . Yii::t('app', 'Restore'), ['restore', 'id' => $model->id], [
                                'class' => 'btn btn-success',
                                'data' => [
                                    'confirm' => Module::t('app', 'You are sure you want to recover the data?'),
                                    'method' => 'post',
                                ],
                            ]) ?>
                        <?php else: ?>
                            <?= Html::a('<i class="fa fa-trash" aria-hidden="true"></i>&nbsp;' . Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                                'class' => 'btn btn-danger',
                                'data' => [
                                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                    'method' => 'post',
                                ],
                            ]) ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
