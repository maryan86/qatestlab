<?php

use common\models\User;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Pjax;
use \backend\modules\catalogue\Module;
use \backend\modules\catalogue\enums\StatusCompany;
use backend\modules\catalogue\models\CompanyTenders;
use \yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Module::t('app', 'List of Tenders');
$this->params['breadcrumbs'][] = ['label' => Module::t('app', 'Catalogue'), 'url' => ['/catalogue', 'lang' => $data->lang]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="page-wrapper" class="list-companies">
    <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <?php try {
                echo \yii\widgets\Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]);
            } catch (Exception $e) {
            } ?>

            <?php Pjax::begin(['id' => 'table_tenders', 'enablePushState' => false]); ?>

            <div class="panel panel-default">
                <div class="block-title panel-heading">
                    <div class="col-md-2">
                        <?= $this->title; ?>
                    </div>
                </div>

                <div class="panel-body nopadding-top">
                    <?php
                    echo GridView::widget([
                        'dataProvider' => $dataProvider,
                        'layout' => '{items}<div class="col-sm-4 nopadding">{summary}</div>
                            <div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate">{pager}</div>',
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            'id',
                            [
                                'attribute' => 'name',
                                'value' => function ($model) use ($data) {
                                    $name = '<b>' . $model->name . '</b>';
                                    $direction = CompanyTenders::getDirectionItem($model->direction->tenders_direction_pid, $data->lang);
                                    if (!empty($direction->name)) {
                                        $name .= '<br>' . $direction->name;
                                    }
                                    $specializationIDs = ArrayHelper::getColumn($model->specialization, 'tenders_specialization_pid');
                                    $specializations = CompanyTenders::getSpecializationItems($specializationIDs, $data->lang);
                                    if (!empty($direction->name)) {
                                        $name .= '<br>' . implode(', ', ArrayHelper::getColumn($specializations, 'name')) . '<br>';
                                    }
                                    $name .= $model->quantity_min == $model->quantity_max ? Yii::$app->formatter->asDecimal($model->quantity_min, 0) : Yii::$app->formatter->asDecimal($model->quantity_min, 0) . ' - ' . Yii::$app->formatter->asDecimal($model->quantity_max, 0);
                                    $name .= Yii::$app->params['capacityType'][Yii::$app->lang->get()][$model->quantity_type];
                                    return Html::a($name, Url::to(['view', 'id' => $model->id]), ['data-pjax' => 0]);
                                },
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'user_id',
                                'value' => function ($model) {
                                    if ($model->user_id) {
                                        $user = User::findOne($model->user_id);
                                        if ($user) {
                                            $name = $user->firstname . ' ' . $user->lastname;
                                            return Html::a(Html::encode($name), Url::toRoute(['/authusers/view', 'id' => $user->id]), ['data-pjax' => 0]);
                                        } else return Yii::t('app', 'User deleted') . ' (id ' . $model->user_id . ')';
                                    }
                                    return Yii::$app->user->getisGuest();
                                },
                                'format' => 'raw',
                            ],
                            'name_customer',
                            'contact_person',
                            'contact_phone',
                            'contact_email',
                            [
                                'attribute' => 'created_at',
                                'value' => function ($model) {
                                    return Yii::$app->formatter->asDatetime($model->created_at, 'medium');
                                },
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'updated_at',
                                'value' => function ($model) {
                                    return Yii::$app->formatter->asDatetime($model->updated_at, 'medium');
                                },
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'status',
                                'value' => function ($model) {
                                    $status = \backend\modules\catalogue\enums\StatusTenders::getStatusAdmin($model->status);
                                    if ($model->status == \backend\modules\catalogue\enums\StatusTenders::TRASH) {
                                        $status .= Html::a('<span class="fa fa-undo" aria-hidden="true"></span>', Url::toRoute(['restore', 'id' => $model->id]), ['data-pjax' => 0, 'data-confirm' => Module::t('app', 'You are sure you want to recover the data?'), 'data-method' => 'post', 'class' => 'restore']);
                                    }
                                    return $status;
                                },
                                'label' => Yii::t('app', 'Status'),
                                'format' => 'raw',
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'buttons' => [
                                    'update' => function () {
                                        return false;
                                    },
                                ],
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
            <?php Pjax::end(); ?>

        </div>
    </div>
</div>
<style>
    .restore:hover {
        text-decoration: none;
    }

    .restore {
        display: block;
        margin-left: 10px;
    }
</style>