<?php

use backend\modules\catalogue\Module;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model backend\modules\catalogue\models\Region */

$this->title = Module::t('app', 'Create Region');
$this->params['breadcrumbs'][] = ['label' => Module::t('app', 'Catalogue'), 'url' => ['/catalogue', 'lang' => $data->lang]];
$this->params['breadcrumbs'][] = ['label' => Module::t('app', 'Regions'), 'url' => ['index', 'lang' => $data->lang]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <?php echo \yii\widgets\Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]); ?>

            <?php Pjax::begin(); ?>

            <div class="panel panel-default">
                <div class="block-title panel-heading">
                    <div class="col-md-6">
                        <?= Html::encode($this->title) ?>
                    </div>
                    <div class="col-md-6">
                        <?php echo $this->render("/catalogue/langLayouts", ['data' => $data]) ?>
                    </div>
                </div>
                <div class="panel-body nopadding-top">
                    <?= $this->render('_form', [
                        'model' => $model,
                        'data' => $data,
                    ]) ?>
                </div>
            </div>

            <?php Pjax::end(); ?>

        </div>
    </div>
</div>