<?php

use backend\modules\catalogue\enums\Status;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\catalogue\models\Region */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="region-form">

    <div class="col-md-12 nopadding">

        <?php $form = ActiveForm::begin(); ?>

        <div class="col-md-6 nopadding">

            <?= $form->field($model, 'lang')->hiddenInput(['maxlength' => true, 'value' => $data->lang])->label(false) ?>

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        </div>

        <div class="nopadding col-md-12">
            <div class="col-md-6 settings-form">
                <?php $model->status = $model->isNewRecord ? 100 : $model->status; ?>
                <?= $form->field($model, 'status',
                    ['options' => [
                        'class' => 'select-status col-md-3 nopadding-left'
                    ]])
                    ->dropDownList(
                        Status::renderStatus(),
                        [
                            'prompt' => '',
                            'options' =>
                                [
                                    500 => ['disabled' => $model->isNewRecord ? true : false]
                                ]
                        ]
                    );
                ?>

                <div class="form-group button-group">
                    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'button fa fa-floppy-o blue']) ?>
                </div>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>

</div>