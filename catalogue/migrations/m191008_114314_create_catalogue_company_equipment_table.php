<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%catalogue_company_equipment}}`.
 */
class m191008_114314_create_catalogue_company_equipment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%catalogue_company_equipment}}', [
            'id' => $this->primaryKey(),
            'catalogue_company_id' => $this->integer(11)->notNull(),
            'catalogue_equipment_pid' => $this->integer(11)->notNull(),
            'check_equipment' => $this->tinyInteger(1)->null(),
            'created_at' => $this->integer(11)->null(),
            'updated_at' => $this->integer(11)->null(),
        ]);

        $this->createIndex(
            'idx-catalogue_company_id',
            '{{%catalogue_company_equipment}}',
            'catalogue_company_id'
        );

        $this->createIndex(
            'idx-catalogue_equipment_pid',
            '{{%catalogue_company_equipment}}',
            'catalogue_equipment_pid'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%catalogue_company_equipment}}');
    }
}
