<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%catalogue_equipment}}`.
 */
class m191001_153921_create_catalogue_equipment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%catalogue_equipment}}', [
            'id' => $this->primaryKey(),
            'p_id' => $this->integer(11)->null(),
            'lang' => $this->string(3)->notNull(),
            'name' => $this->string(180)->notNull(),
            'image' => $this->integer(11)->null(),
            'status' => $this->integer(4)->notNull(),
            'created_at' => $this->integer(11)->null(),
            'updated_at' => $this->integer(11)->null(),
        ]);

        $this->createIndex(
            'idx-equipment-p_id',
            '{{%catalogue_equipment}}',
            'p_id'
        );

        $this->createIndex(
            'idx-equipment-lang',
            '{{%catalogue_equipment}}',
            'lang'
        );

        $this->createIndex(
            'idx-equipment-status',
            '{{%catalogue_equipment}}',
            'status'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%catalogue_equipment}}');
    }
}
