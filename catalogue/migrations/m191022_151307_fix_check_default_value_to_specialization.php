<?php

use yii\db\Migration;

/**
 * Class m191022_151307_fix_check_default_value_to_specialization
 */
class m191022_151307_fix_check_default_value_to_specialization extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%catalogue_company_specialization}}', 'main_specialization',  $this->tinyInteger(1)->defaultValue(0));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('{{%catalogue_company_specialization}}', 'main_specialization',  $this->tinyInteger(1)->null());

    }
}
