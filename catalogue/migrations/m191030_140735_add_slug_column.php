<?php

use yii\db\Migration;

/**
 * Class m191030_140735_add_slug_column
 */
class m191030_140735_add_slug_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%catalogue_company}}', 'slug', $this->string(100)->after('user_id'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%catalogue_company}}', 'slug');
    }
}
