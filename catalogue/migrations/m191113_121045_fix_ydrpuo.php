<?php

use yii\db\Migration;

/**
 * Class m191113_121045_fix_ydrpuo
 */
class m191113_121045_fix_ydrpuo extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%catalogue_company}}', 'edrpou', $this->string(8)->null());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('{{%catalogue_company}}', 'edrpou', $this->integer(8)->null());

    }
}
