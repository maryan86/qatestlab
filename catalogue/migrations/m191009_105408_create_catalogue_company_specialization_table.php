<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%catalogue_company_specialization}}`.
 */
class m191009_105408_create_catalogue_company_specialization_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%catalogue_company_specialization}}', [
            'id' => $this->primaryKey(),
            'catalogue_company_id' => $this->integer(11)->notNull(),
            'catalogue_direction_pid' => $this->integer(11)->notNull(),
            'catalogue_specialization_pid' => $this->integer(11)->notNull(),
            'main_specialization' => $this->tinyInteger(1)->null(),
            'check_specialization' => $this->tinyInteger(1)->null(),
            'created_at' => $this->integer(11)->null(),
            'updated_at' => $this->integer(11)->null(),
        ]);

        $this->createIndex(
            'idx-catalogue_company_id',
            '{{%catalogue_company_specialization}}',
            'catalogue_company_id'
        );

        $this->createIndex(
            'idx-catalogue_equipment_pid',
            '{{%catalogue_company_specialization}}',
            'catalogue_direction_pid'
        );

        $this->createIndex(
            'idx-catalogue_specialization_pid',
            '{{%catalogue_company_specialization}}',
            'catalogue_specialization_pid'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%catalogue_company_specialization}}');
    }
}
