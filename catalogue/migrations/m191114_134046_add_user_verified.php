<?php

use yii\db\Migration;

/**
 * Class m191114_134046_add_user_verified
 */
class m191114_134046_add_user_verified extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%catalogue_company}}', 'user_verified', $this->integer(11)->null()->after('date_verified'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%catalogue_company}}', 'user_verified');
    }

}
