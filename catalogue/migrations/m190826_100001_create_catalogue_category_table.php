<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%catalogue_category}}`.
 */
class m190826_100001_create_catalogue_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%catalogue_category}}', [
            'id' => $this->primaryKey(),
            'p_id' => $this->integer(11)->null(),
            'lang' => $this->string(3)->notNull(),
            'direction_id' => $this->integer(11)->null(),
            'name' => $this->string(180)->notNull(),
            'status' => $this->integer(4)->notNull(),
            'created_at' => $this->integer(11)->null(),
            'updated_at' => $this->integer(11)->null(),
        ]);

        $this->createIndex(
            'idx-category-p_id',
            '{{%catalogue_category}}',
            'p_id'
        );

        $this->createIndex(
            'idx-category-lang',
            '{{%catalogue_category}}',
            'lang'
        );

        $this->createIndex(
            'idx-category-direction_id',
            '{{%catalogue_category}}',
            'direction_id'
        );

        $this->createIndex(
            'idx-category-status',
            '{{%catalogue_category}}',
            'status'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%catalogue_category}}');
    }
}
