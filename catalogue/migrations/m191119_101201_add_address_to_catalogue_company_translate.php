<?php

use yii\db\Migration;

/**
 * Class m191119_101201_add_address_to_catalogue_company_translate
 */
class m191119_101201_add_address_to_catalogue_company_translate extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%catalogue_company_translate}}', 'address', $this->string(255)->null()->after('description'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%catalogue_company_translate}}', 'address');
    }
}