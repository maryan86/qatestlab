<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%catalogue_specialization}}`.
 */
class m190827_140523_create_catalogue_specialization_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%catalogue_specialization}}', [
            'id' => $this->primaryKey(),
            'p_id' => $this->integer(11)->null(),
            'lang' => $this->string(3)->notNull(),
            'name' => $this->string(180)->notNull(),
            'status' => $this->integer(4)->notNull(),
            'direction_id' => $this->integer(11)->null(),
            'category_id' => $this->integer(11)->null(),
            'filter_id' => $this->integer(11)->null(),
            'created_at' => $this->integer(11)->null(),
            'updated_at' => $this->integer(11)->null(),
        ]);

        $this->createIndex(
            'idx-specialization-p_id',
            '{{%catalogue_specialization}}',
            'p_id'
        );

        $this->createIndex(
            'idx-specialization-direction_id',
            '{{%catalogue_specialization}}',
            'direction_id'
        );

        $this->createIndex(
            'idx-specialization-category_id',
            '{{%catalogue_specialization}}',
            'category_id'
        );

        $this->createIndex(
            'idx-specialization-filter_id',
            '{{%catalogue_specialization}}',
            'filter_id'
        );

        $this->createIndex(
            'idx-specialization-lang',
            '{{%catalogue_specialization}}',
            'lang'
        );

        $this->createIndex(
            'idx-specialization-status',
            '{{%catalogue_specialization}}',
            'status'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%catalogue_specialization}}');
    }
}
