<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%catalogue_company_translate}}`.
 */
class m191008_103516_create_catalogue_company_translate_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%catalogue_company_translate}}', [
            'id' => $this->primaryKey(),
            'catalogue_company_id' => $this->integer(11)->notNull(),
            'lang' => $this->string(3)->notNull(),
            'name' => $this->string(255)->notNull(),
            'description' => $this->text()->null(),
            'city' => $this->string(100)->null(),
            'created_at' => $this->integer(11)->null(),
            'updated_at' => $this->integer(11)->null(),
        ]);

        $this->createIndex(
            'idx-catalogue_company_id',
            '{{%catalogue_company_translate}}',
            'catalogue_company_id'
        );

        $this->createIndex(
            'idx-company-name',
            '{{%catalogue_company_translate}}',
            'name'
        );

        $this->createIndex(
            'idx-company-city',
            '{{%catalogue_company_translate}}',
            'city'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%catalogue_company_translate}}');
    }
}
