<?php

use yii\db\Migration;

/**
 * Class m191021_140704_change_company_phone
 */
class m191021_140704_change_company_phone extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%catalogue_company}}', 'contact_phone', $this->text()->notNull());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('{{%catalogue_company}}', 'contact_phone', $this->string(20)->null());

    }

}
