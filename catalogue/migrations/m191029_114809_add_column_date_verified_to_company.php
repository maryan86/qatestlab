<?php

use yii\db\Migration;

/**
 * Class m191029_114809_add_column_date_verified_to_company
 */
class m191029_114809_add_column_date_verified_to_company extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%catalogue_company}}', 'date_verified', $this->integer(11)->null()->after('user_id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%catalogue_company}}', 'date_verified');
    }
}