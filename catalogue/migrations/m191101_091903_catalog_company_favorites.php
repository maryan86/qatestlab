<?php

use yii\db\Migration;

/**
 * Class m191101_091903_catalog_company_favorites
 */
class m191101_091903_catalog_company_favorites extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%catalogue_company_favorites}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer(11)->null(),
            'user_id' => $this->integer(11)->null(),
            'created_at' => $this->integer(11)->null(),
            'updated_at' => $this->integer(11)->null(),
        ]);

        $this->createIndex(
            'idx-company-user_id',
            '{{%catalogue_company_favorites}}',
            'user_id'
        );

        $this->createIndex(
            'idx-company-company_id',
            '{{%catalogue_company_favorites}}',
            'company_id'
        );

        $this->createIndex(
            'idx-company-user_id-company_id',
            '{{%catalogue_company_favorites}}',
            [
                'company_id',
                'user_id',
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%catalogue_company_favorites}}');
    }
}
