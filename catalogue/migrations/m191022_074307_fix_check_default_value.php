<?php

use yii\db\Migration;

/**
 * Class m191022_074307_fix_check_default_value
 */
class m191022_074307_fix_check_default_value extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%catalogue_company_equipment}}', 'check_equipment',  $this->tinyInteger(1)->defaultValue(0));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('{{%catalogue_company_equipment}}', 'check_equipment',  $this->tinyInteger(1)->null());

    }
}
