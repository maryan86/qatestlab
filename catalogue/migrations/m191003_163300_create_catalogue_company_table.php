<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%catalogue_company}}`.
 */
class m191003_163300_create_catalogue_company_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%catalogue_company}}', [
            'id' => $this->primaryKey(),
            'email' => $this->string(50)->null(),
            'edrpou' => $this->integer(8)->null(),
            'site' => $this->string(100)->null(),
            'phone' => $this->string(20)->null(),
            'year' => $this->integer(4)->null(),
            'contact_person' => $this->string(100)->null(),
            'contact_email' => $this->string(50)->null(),
            'contact_phone' => $this->string(20)->null(),
            'region_pid' => $this->integer(11)->null(),
            'logo' => $this->string(255)->null(),
            'photos' => $this->text()->null(),
            'capacity_min' => $this->integer(11)->null(),
            'capacity_max' => $this->integer(11)->null(),
            'capacity_type' => $this->integer(2)->null(),
            'personnel_quantity' => $this->integer(11)->null(),
            'tailor_made' => $this->tinyInteger(1)->null(),
            'bank_payment' => $this->tinyInteger(1)->null(),
            'cash_payment' => $this->tinyInteger(1)->null(),
            'vat_payment' => $this->tinyInteger(1)->null(),
            'verified' => $this->tinyInteger(1)->null(),
            'associated' => $this->tinyInteger(1)->null(),
            'check_location' => $this->tinyInteger(1)->null(),
            'check_personnel_quantity' => $this->tinyInteger(1)->null(),
            'check_year' => $this->tinyInteger(1)->null(),
            'check_site' => $this->tinyInteger(1)->null(),
            'check_capacity' => $this->tinyInteger(1)->null(),
            'status' => $this->integer(4)->notNull(),
            'user_id' => $this->integer(11)->notNull(),
            'created_at' => $this->integer(11)->null(),
            'updated_at' => $this->integer(11)->null(),
        ]);

        $this->createIndex(
            'idx-company-verified',
            '{{%catalogue_company}}',
            'verified'
        );

        $this->createIndex(
            'idx-company-associated',
            '{{%catalogue_company}}',
            'associated'
        );

        $this->createIndex(
            'idx-company-status',
            '{{%catalogue_company}}',
            'status'
        );

        $this->createIndex(
            'idx-company-created_at',
            '{{%catalogue_company}}',
            'created_at'
        );

        $this->createIndex(
            'idx-company-year',
            '{{%catalogue_company}}',
            'year'
        );

        $this->createIndex(
            'idx-company-personnel_quantity',
            '{{%catalogue_company}}',
            'personnel_quantity'
        );

        $this->createIndex(
            'idx-company-user_id',
            '{{%catalogue_company}}',
            'user_id'
        );

        $this->createIndex(
            'idx-company-capacity',
            '{{%catalogue_company}}',
            [
                'capacity_min',
                'capacity_max',
                'capacity_type',
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%catalogue_company}}');
    }
}
