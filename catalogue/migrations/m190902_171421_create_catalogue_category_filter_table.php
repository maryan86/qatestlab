<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%catalogue_category_filter}}`.
 */
class m190902_171421_create_catalogue_category_filter_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%catalogue_category_filter}}', [
            'id' => $this->primaryKey(),
            'lang' => $this->string(3)->notNull(),
            'name' => $this->string(180)->notNull(),
            'catalogue_category_id' => $this->integer(11)->notNull(),
            'created_at' => $this->integer(11)->null(),
            'updated_at' => $this->integer(11)->null(),
        ]);

        $this->createIndex(
            'idx-category_filter-catalogue_category_id',
            '{{%catalogue_category_filter}}',
            'catalogue_category_id'
        );

        $this->addForeignKey('fk_filter-catalogue_category_catalogue_category_id', '{{%catalogue_category_filter}}', 'catalogue_category_id', '{{%catalogue_category}}', 'id', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%catalogue_category_filter}}');
    }
}
