<?php

use yii\db\Migration;

/**
 * Class m191029_100001_add_image_to_direction
 */
class m191029_100001_add_image_to_direction extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%catalogue_direction}}', 'image', $this->integer(11)->null()->after('name'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%catalogue_direction}}', 'image');
    }
}