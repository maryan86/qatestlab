<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%catalogue_region}}`.
 */
class m190925_135825_create_catalogue_region_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%catalogue_region}}', [
            'id' => $this->primaryKey(),
            'p_id' => $this->integer(11)->null(),
            'lang' => $this->string(3)->notNull(),
            'name' => $this->string(180)->notNull(),
            'status' => $this->integer(4)->notNull(),
            'created_at' => $this->integer(11)->null(),
            'updated_at' => $this->integer(11)->null(),
        ]);

        $this->createIndex(
            'idx-region-p_id',
            '{{%catalogue_region}}',
            'p_id'
        );

        $this->createIndex(
            'idx-region-lang',
            '{{%catalogue_region}}',
            'lang'
        );

        $this->createIndex(
            'idx-region-status',
            '{{%catalogue_region}}',
            'status'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%catalogue_region}}');
    }
}
