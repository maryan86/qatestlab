<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%catalogue_direction}}`.
 */
class m190820_120025_create_catalogue_direction_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%catalogue_direction}}', [
            'id' => $this->primaryKey(),
            'p_id' => $this->integer(11)->null(),
            'lang' => $this->string(3)->notNull(),
            'name' => $this->string(180)->notNull(),
            'status' => $this->integer(4)->notNull(),
            'created_at' => $this->integer(11)->null(),
            'updated_at' => $this->integer(11)->null(),
        ]);

        $this->createIndex(
            'idx-direction-p_id',
            '{{%catalogue_direction}}',
            'p_id'
        );

        $this->createIndex(
            'idx-direction-lang',
            '{{%catalogue_direction}}',
            'lang'
        );

        $this->createIndex(
            'idx-direction-status',
            '{{%catalogue_direction}}',
            'status'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%catalogue_direction}}');
    }
}
