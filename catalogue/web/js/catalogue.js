$(document).ready(function () {
    $(document).on('click', '#add_filter', function (e) {
        e.preventDefault();
        let filterInput = '<div class="form-group field-category-filter">\n' +
                '<input type="text" class="form-control category-filter display-inline" name="Category[filter][]">\n' +
                '<a class="btn btn-danger display-inline remove_filter" title="Видалити"><span class="glyphicon -minus glyphicon-minus"></span></a>\n' +
            '</div>';
        $(this).before(filterInput);
    });

    $(document).on('click', '.remove_filter', function (e) {
        e.preventDefault();
        $(this).parent('.field-category-filter').remove();
    });

    $(document).on('change', '#specialization-direction_id', function (e) {
        e.preventDefault();
        let directionId = $(this).val();
        if(!directionId){
            $('.field-specialization-category_id').hide(); $('.field-specialization-filter_id').hide();
            return false;
        }
        $.post("/control/catalogue/category/direction/" + directionId, function () {
        }).done(function (data) {
            let specializationCategory = $('#specialization-category_id');
            specializationCategory.html('').show();
            if (data) {
                $('.field-specialization-category_id').show();
                specializationCategory.append($("<option></option>"));
                $.each(data, function (key, value) {
                    $('#specialization-category_id')
                        .append($("<option></option>")
                            .attr("value", key)
                            .text(value));
                });
            } else $('.field-specialization-category_id').hide(); $('.field-specialization-filter_id').hide();
        });
    });

    $(document).on('change', '#specialization-category_id', function (e) {
        e.preventDefault();
        let categoryId = $(this).val();
        if(!categoryId){
            $('.field-specialization-filter_id').hide();
            return false;
        }
        $.post("/control/catalogue/category/filter/" + categoryId, function () {
        }).done(function (data) {
            let specializationCategory = $('#specialization-filter_id');
            specializationCategory.html('').show();
            if (data) {
                $('.field-specialization-filter_id').show();
                specializationCategory.append($("<option></option>"));
                $.each(data, function (key, value) {
                    $('#specialization-filter_id')
                        .append($("<option></option>")
                            .attr("value", key)
                            .text(value));
                });
            } else $('.field-specialization-filter_id').hide();
        });
    });
    $(document).on('click', '#logo_container a.delete', function (e) {
        e.preventDefault();
        if ($('#logo_container input').length == 0) {
            $('#logo_container').removeAttr('style');
            $('#logo_container').html('<input type="file" id="logo" name="logo" class="formInput">');
            $('#logo_img input').val('');

        }

    });
    $(document).on('change', 'input#logo', function () {
        console.log(123);
        var data = new FormData();
        var file = $(this)[0].files[0];
        data.append('file', file);
        $.ajax({
            url: "/control/catalogue/upload/logo",
            data: data,
            type: 'POST',
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                if (response) {
                    if (typeof response.image != 'undefined') {
                        if ($('#logo_container img').length == 0) {
                            $('#logo_container').html('<img src="' + response.image + '"><a href="#" class="delete">+</a>');
                            $('#logo_img input').val(response.image);
                            makeBackgroundFromImg($('#logo_container'));
                        }
                    }

                }
            }
        });
    });
    $(document).on('click', '#company_photos a.delete', function (e) {
        e.preventDefault();
        var imgOrigin = $(this).parent().find('img').attr('data-origin');
        $(this).parent().remove();
        var images = JSON.parse($('#logo_photos input').val());
        var delValue;
        $.each(images, function (index, value) {
            if (value.origin == imgOrigin) {
                delValue = value;
            }
        });
        if (delValue) {
            images.remove(delValue);
        }
        $('#logo_photos input').val(JSON.stringify(images));
    });
    $(document).on('change', 'input#photos', function () {
        var data = new FormData();
        var files = $(this)[0].files;
        for (var i = 0; i < files.length; i++) {
            data.append('files[' + i + ']', files[i]);
        }
        $.ajax({
            url: "/control/catalogue/upload/photos",
            data: data,
            type: 'POST',
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                if (response) {
                    if (typeof response.images != 'undefined') {
                        $.each(response.images, function (index, value) {
                            var img;
                            if (typeof value.crop_2 != 'undefined') {
                                img = value.crop_2;
                            } else {
                                img = value.origin;
                            }
                            $('#company_photos .profile_input_file.add').before('<div class="profile_input_file"><img data-origin="' + value.origin + '" src="' + img + '"><a href="#" class="delete">+</a></div>');

                        });
                        var imageContainer = $('#company_photos .profile_input_file').not('.add');
                        $.each(imageContainer, function(i, e) {
                            makeBackgroundFromImg(e);
                        });
                        var data = [];
                        if ($('#logo_photos input').val()) {
                            data = JSON.parse($('#logo_photos input').val());
                        }
                        $('#logo_photos input').val(JSON.stringify(data.concat(response.images)));
                    }
                }
            }
        });
    });
    var block =  $('.profile_input_file');
    $.each(block, function(i,e) {
        makeBackgroundFromImg($(e));
    });

});
Array.prototype.remove = function () {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};
function makeBackgroundFromImg(block) {
    if($(block).find('img').length != 0 ){
        var img = $(block).children('img');
        var src = img.attr('src');
        img.hide();

        $(block).css({'background': 'url("'+src+'") 50% 50% no-repeat'});
        if($(block).is('#logo_container')){
            $(block).css({'background-size': 'cover'})
        }
    }
}