<?php

namespace backend\modules\catalogue\assets;

use Yii;
use yii\web\AssetBundle;
use yii\helpers\Json;

/**
 * Class CatalogueAssets
 * @package backend\modules\catalogue\assets
 */
class CatalogueAssets extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@backend/modules/catalogue/web';

    /**
     * @var array
     */
    public $publishOptions = ['forceCopy' => true];

    /**
     * @var array
     */
    public $css = [
       'css/catalogue.css'
    ];

    /**
     * @var array
     */
    public $js = [
        'js/catalogue.js'
    ];

    /**
     * @var array
     */
    public $jsOptions = ['position' => \yii\web\View::POS_END];

    /**
     * @var array
     */
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'backend\assets\AppAsset',
    ];

    public function init()
    {
        parent::init();
        $dirMassage = Yii::getAlias("@modules") . '/catalogue/messages/';
        $fileMap = (object)Yii::$app->i18n->translations['modules/catalogue/*'];
        $messages = $this->loadMessagesFromFile($dirMassage, $fileMap->fileMap, Yii::$app->language);
        if ($messages) {
            Yii::$app->view->registerJs("var moduleCatalogue = " . $messages, \yii\web\View::POS_HEAD, 'module-catalogue');
        }
    }

    /**
     * Register transliteration global variables to javascript.
     *
     * @param $messageDir
     * @param $fileMap
     * @param $language
     * @return string|null
     */
    protected function loadMessagesFromFile($messageDir, $fileMap, $language)
    {
        $messages = [];
        $fallbackLanguage = substr($language, 0, 2);

        foreach ($fileMap as $map) {
            $pos = strpos($map, ".");
            $mapName = substr($map, 0, $pos);
            $messageFile = $messageDir . $fallbackLanguage . '/' . $map;
            if (is_file($messageFile)) {
                $messagesFile = include($messageFile);
                $keys = str_replace(' ', '', array_keys($messagesFile));
                $values = array_values($messagesFile);
                $messages[$mapName] = array_combine($keys, $values);
                if (!is_array($messages)) {
                    $messages[$mapName] = [];
                }
            }
        }
        if ($messages) {
            return Json::encode($messages);
        } else {
            return null;
        }
    }
}
