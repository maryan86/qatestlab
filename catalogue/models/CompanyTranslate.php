<?php

namespace backend\modules\catalogue\models;

use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%catalogue_company_translate}}".
 *
 * @property int $id
 * @property int $catalogue_company_id
 * @property string $lang
 * @property string $name
 * @property string $description
 * @property string $address
 * @property string $city
 * @property int $check_equipment
 * @property int $created_at
 * @property int $updated_at
 */
class CompanyTranslate extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%catalogue_company_translate}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => function () {
                    return date('U');
                },
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['catalogue_company_id', 'lang'], 'required'],
            [['catalogue_company_id', 'created_at', 'updated_at'], 'integer'],
            [['description'], 'string'],
            [['lang'], 'string', 'max' => 3],
            [['name', 'address'], 'string', 'max' => 255],
            [['city'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'catalogue_company_id' => 'Catalogue Company ID',
            'lang' => 'Lang',
            'name' => 'Name',
            'description' => 'Description',
            'address' => 'Address',
            'city' => 'City',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * related table Company
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyInfo()
    {
        return $this->hasOne(Company::class, ['id' => 'catalogue_company_id']);
    }
}
