<?php

namespace backend\modules\catalogue\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%catalogue_company_equipment}}".
 *
 * @property int $id
 * @property int $catalogue_company_id
 * @property int $catalogue_equipment_pid
 * @property int $check_equipment
 * @property int $created_at
 * @property int $updated_at
 */
class CompanyEquipment extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%catalogue_company_equipment}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => function () {
                    return date('U');
                },
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['catalogue_company_id', 'catalogue_equipment_pid'], 'required'],
            [['catalogue_company_id', 'catalogue_equipment_pid', 'check_equipment', 'created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'catalogue_company_id' => Yii::t('app', 'Catalogue Company ID'),
            'catalogue_equipment_pid' => Yii::t('app', 'Catalogue Equipment Pid'),
            'check_equipment' => Yii::t('app', 'Check Equipment'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @param $companyId
     * @param $filtersIn
     * @param array $check
     * @return bool
     */
    public static function updateEquipment($companyId, $filtersIn, $check = [])
    {
        if (!empty($companyId)) {
            self::deleteAll(['catalogue_company_id' => $companyId]);
        }
        if ($filtersIn) {
            foreach ($filtersIn as $item) {
                $modelCategoryFilter = new self();
                $modelCategoryFilter->catalogue_equipment_pid = $item;
                $modelCategoryFilter->catalogue_company_id = $companyId;
                $modelCategoryFilter->check_equipment = !empty($check[$item]) ? 1 : 0;
                $modelCategoryFilter->save();
            }
            return true;
        }
        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEquipment()
    {
        return $this->hasOne(Equipment::class, ['p_id' => 'catalogue_equipment_pid']);
    }
}
