<?php

namespace backend\modules\catalogue\models;

use backend\modules\catalogue\enums\StatusCompany;
use backend\modules\catalogue\Module;
use common\models\User;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class CompanySearch extends Model
{

    public $user_id;
    public $name;
    public $email;
    public $contact_phone;
    public $contact_email;
    public $verified;
    public $associated;
    public $region;
    public $status;
    public $date;
    public $user;
    public $edrpou;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'email', 'contact_phone', 'contact_email', 'user', 'edrpou'], 'string'],
            [['user_id', 'status', 'region'], 'integer'],
            [['verified', 'associated'], 'boolean'],
            [['date'], 'date', 'format' => 'php:Y-m-d'],

        ];
    }

    public function attributeLabels()
    {
        return [
            'verified' => Module::t('app', 'Verified'),
            'associated' => Module::t('app', 'Associated'),
        ];
    }

    /**
     * Search provider
     *
     * @return ActiveDataProvider
     */

    public function search()
    {
        $query = Company::find()->joinWith(['user', 'info']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query->distinct(),
            'sort' => [
                'defaultOrder' => ['created_at' => SORT_DESC, 'id' => SORT_DESC]
            ],
        ]);
        if (!$this->validate()) {
            return $dataProvider;
        }
        $query->andFilterWhere([
            Company::tableName() . '.user_id' => $this->user_id,
            Company::tableName() . '.status' => $this->status,
            Company::tableName() . '.region_pid' => $this->region,
        ]);
        if (!empty($this->verified)) {
            $query->andFilterWhere([
                Company::tableName() . '.verified' => $this->verified,
            ]);
        }
        if (!empty($this->associated)) {
            $query->andFilterWhere([
                Company::tableName() . '.associated' => $this->associated,
            ]);
        }
        $query->andFilterWhere(['and',
            ['like', 'name', $this->name],
            ['like', Company::tableName() . '.email', $this->email],
            ['like', Company::tableName() . '.contact_phone', $this->contact_phone],
            ['like', Company::tableName() . '.contact_email', $this->contact_email],
            ['like', Company::tableName() . '.edrpou', $this->edrpou],
        ]);
        $query->andFilterWhere(['or',
            ['like', User::tableName() . '.firstname', $this->user],
            ['like', User::tableName() . '.lastname', $this->user],
        ]);
        if (!empty($this->date)) {
            $dateFrom = strtotime($this->date . ' 00:00:00');
            $dateTo = strtotime($this->date . ' 23:59:59');
            $query->andFilterWhere(['and',
                ['>=', Company::tableName() . '.created_at', $dateFrom],
                ['<=', Company::tableName() . '.created_at', $dateTo],
            ]);
        }
        if (empty($this->status)) {
            $query->andWhere(['<>', Company::tableName() . '.status', StatusCompany::TRASH]);
        }
        return $dataProvider;

    }
}