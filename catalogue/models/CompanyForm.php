<?php

namespace backend\modules\catalogue\models;

use backend\modules\catalogue\enums\StatusCompany;
use backend\modules\catalogue\enums\StatusTenders;
use backend\modules\catalogue\models\Company;
use backend\modules\catalogue\models\CompanySpecialization;
use backend\modules\catalogue\models\CompanyTranslate;
use backend\modules\catalogue\Module;
use common\models\User;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * Class CompanyForm
 * @package backend\modules\catalogue\models
 */
class CompanyForm extends Model
{
    public $id;
    public $lang;
    public $name = [];
    public $city = [];
    public $address = [];
    public $description = [];
    public $main_direction;
    public $main_specialization;
    public $additional_specializations;
    public $email;
    public $edrpou;
    public $site;
    public $region_pid;
    public $year;
    public $contact_person;
    public $contact_email;
    public $contact_phone;
    public $logo;
    public $photos;
    public $status;
    public $phone;
    public $capacity_min;
    public $capacity_max;
    public $capacity_type;
    public $personnel_quantity;
    public $tailor_made;
    public $bank_payment;
    public $cash_payment;
    public $vat_payment;
    public $verified;
    public $associated;
    public $check_personnel_quantity;
    public $check_location;
    public $check_year;
    public $check_site;
    public $check_capacity;
    public $check_specialization;
    public $check_additional_specialization;
    public $translate = [];
    public $equipment =[];
    public $check_equipment;
    public $date_verified;
    public $user_id;
    public $slug;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['year', 'edrpou', 'region_pid', 'capacity_min', 'capacity_max', 'capacity_type',
                'personnel_quantity', 'tailor_made', 'bank_payment', 'cash_payment', 'vat_payment', 'verified', 'associated',
                'check_location', 'check_personnel_quantity', 'check_year', 'check_site', 'check_capacity', 'check_specialization', 'check_additional_specialization',
                'status', 'main_direction', 'date_verified', 'user_id'], 'integer'],
            [['photos', 'description'], 'safe'],
            [['name', 'city'], 'safe'],
            [['description'], 'string', 'max' => 2000],
            //[['contact_phone','status', 'edrpou', 'email', 'region_pid','main_specialization', 'main_direction', 'contact_person', 'contact_email', 'year'], 'required'],
            [['status','email'], 'required'],
            [['email', 'contact_email'], 'email'],
            [['site', 'contact_person'], 'string', 'max' => 100],
            ['site', 'url', 'defaultScheme' => 'https'],
            [['phone'], 'string', 'max' => 20],
            [['slug'], 'string', 'max' => 100],
            [['logo'], 'string', 'max' => 255],
            [['address'], 'string', 'max' => 100],
            [['lang'], 'default', 'value' => Yii::$app->lang->get()],
            ['contact_phone', 'each', 'rule' => ['match', 'pattern' => '\^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){12,15}(\s*)?$']],
            ['contact_phone', 'each', 'rule' => ['required']],
            ['main_specialization', 'each', 'rule' => ['required']],
            ['main_specialization', 'each', 'rule' => ['integer']],
            ['equipment', 'each', 'rule' => ['integer']],
            ['check_equipment', 'each', 'rule' => ['integer']],
            ['additional_specializations', 'each', 'rule' => ['integer']],
            [['edrpou'], 'string', 'length' => 8],
            ['edrpou', 'match', 'pattern' => '/^\d{8}$/', 'message' => Yii::t('app', 'Field must contain exactly 8 digits.')],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'email' => Module::t('app', 'Company E-mail'),
            'name' => Module::t('app', 'Company Name'),
            'edrpou' => Module::t('app', 'Edrpou code'),
            'site' => Module::t('app', 'Site'),
            'phone' => Module::t('app', 'Company Phone'),
            'year' => Module::t('app', 'How old is the company'),
            'contact_person' => Module::t('app', 'Contact Person'),
            'contact_email' => Module::t('app', 'Contact E-mail'),
            'contact_phone' => Module::t('app', 'Contact Phone'),
            'region_pid' => Module::t('app', 'Region'),
            'city' => Module::t('app', 'City'),
            'address' => Module::t('app', 'Address'),
            'description' => Module::t('app', 'A brief description of the company'),
            'logo' => Module::t('app', 'Logo'),
            'photos' => Module::t('app', 'Photos'),
            'capacity_min' => Module::t('app', 'Min'),
            'capacity_max' => Module::t('app', 'Max'),
            'capacity_type' => Module::t('app', 'Capacity Type'),
            'personnel_quantity' => Module::t('app', 'Personnel Quantity'),
            'tailor_made' => Module::t('app', 'Tailoring from giving materials'),
            'bank_payment' => Module::t('app', 'Cash payment'),
            'cash_payment' => Module::t('app', 'Non-cash payment'),
            'vat_payment' => Module::t('app', 'Payment for VAT'),
            'verified' => Module::t('app', 'Verified Profile'),
            'check_equipment' => Module::t('app', 'Check Equipment'),
            'associated' => Module::t('app', 'Associate Member of Ukrlegprom'),
            'check_location' => Module::t('app', 'Check Location'),
            'check_personnel_quantity' => Module::t('app', 'Check Personnel Quantity'),
            'check_year' => Module::t('app', 'Check Year'),
            'check_site' => Module::t('app', 'Check Site'),
            'check_capacity' => Module::t('app', 'Check Capacity'),
            'check_specialization' => Module::t('app', 'Check Specialization'),
            'check_additional_specialization' => Module::t('app', 'Check Additional Specialization'),
            'status' => Yii::t('app', 'Status'),
            'main_direction' => Module::t('app', 'Direction'),
            'main_specialization' => Module::t('app', 'Specializations'),
            'user_id' => Module::t('app', 'User'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \backend\modules\catalogue\models\Company|bool|null
     */
    public function save()
    {
        if (!empty($this->id)) {
            $model = Company::findOne($this->id);
            //$companyTranslate = CompanyTranslate::find()->where(['catalogue_company_id' => $this->id])->all();
        } else {
            $model = new Company();
            //$modelTranslate = new CompanyTranslate();
        }
        if ($model->load($this->attributes, '')) {
            $phones = array_filter($this->contact_phone, function ($v) {
                return !empty($v);
            });
            $model->contact_phone = Json::encode($phones);

            if(!$model->date_verified && $this->status == StatusCompany::VERIFY){
                $model->date_verified = date("U");
            }
            if ($this->status == StatusCompany::VERIFY) {
                $model->user_verified = Yii::$app->user->id;
            }

            if ($model->save()) {

                $this->id = $model->id;
                foreach ($this->name as $key => $item) {
                    $companyTranslate = CompanyTranslate::findOne(['catalogue_company_id' => $this->id, 'lang' => $key]);
                    if ($companyTranslate) {
                        $companyTranslate->name = trim($item);
                        $companyTranslate->save();
                    } elseif($this->name[$key]) {
                        $modelTranslate = new CompanyTranslate();
                        $modelTranslate->lang = $key;
                        $modelTranslate->catalogue_company_id = $model->id;
                        $modelTranslate->name = trim($item);
                        $modelTranslate->save();
                    }
                }
                foreach ($this->city as $key => $item) {
                    $companyTranslate = CompanyTranslate::findOne(['catalogue_company_id' => $this->id, 'lang' => $key]);
                    if ($companyTranslate) {
                        $companyTranslate->city = $item;
                        $companyTranslate->save();
                    } elseif($this->city[$key]) {
                        $modelTranslate = new CompanyTranslate();
                        $modelTranslate->lang = $key;
                        $modelTranslate->catalogue_company_id = $model->id;
                        $modelTranslate->city = trim($item);
                        $modelTranslate->save();
                    }
                }
                foreach ($this->address as $key => $item) {
                    $companyTranslate = CompanyTranslate::findOne(['catalogue_company_id' => $this->id, 'lang' => $key]);
                    if ($companyTranslate) {
                        $companyTranslate->address = $item;
                        $companyTranslate->save();
                    } elseif($this->address[$key]) {
                        $modelTranslate = new CompanyTranslate();
                        $modelTranslate->lang = $key;
                        $modelTranslate->catalogue_company_id = $model->id;
                        $modelTranslate->address = trim($item);
                        $modelTranslate->save();
                    }
                }
                foreach ($this->description as $key => $item) {
                    $companyTranslate = CompanyTranslate::findOne(['catalogue_company_id' => $this->id, 'lang' => $key]);
                    if ($companyTranslate) {
                        $companyTranslate->description = $item;
                        $companyTranslate->save();
                    } elseif($this->description[$key]) {
                        $modelTranslate = new CompanyTranslate();
                        $modelTranslate->lang = $key;
                        $modelTranslate->catalogue_company_id = $model->id;
                        $modelTranslate->description = trim($item);
                        $modelTranslate->save();
                    }
                }
                CompanySpecialization::deleteAll(['catalogue_company_id' => $this->id, 'main_specialization' => 1]);

                if ($this->main_direction && $this->main_specialization) {
                    foreach ($this->main_specialization as $specialization) {
                        $modelSpecialization = new CompanySpecialization();
                        $modelSpecialization->catalogue_company_id = $this->id;
                        $modelSpecialization->catalogue_direction_pid = $this->main_direction;
                        $modelSpecialization->catalogue_specialization_pid = $specialization;
                        $modelSpecialization->main_specialization = 1;
                        $modelSpecialization->check_specialization = $this->check_specialization;
                        $modelSpecialization->save();
                    }
                }elseif ($this->main_direction && empty($this->main_specialization)){
                    $modelSpecialization = new CompanySpecialization();
                    $modelSpecialization->catalogue_company_id = $this->id;
                    $modelSpecialization->catalogue_direction_pid = $this->main_direction;
                    $modelSpecialization->catalogue_specialization_pid = 0;
                    $modelSpecialization->main_specialization = 1;
                    $modelSpecialization->check_specialization = $this->check_specialization;
                    $modelSpecialization->save();
                }

                CompanyEquipment::updateEquipment($model->id, $this->equipment, $this->check_equipment);
                $this->setAdditional($this->additional_specializations, $model->id);

                if($model->date_verified && $model->status == StatusCompany::VERIFY){
                    CompanyTenders::updateAll(['status' => StatusTenders::ACTIVE],
                        ['and',
                            ['=', 'user_id',  $model->user_id],
                            ['in', 'status', [StatusTenders::COMPANY_TRASH, StatusTenders::USER_DEACTIVATED]],
                        ]);
                    
//                    $userCompany = User::findOne($model->user_id);
//                    if($userCompany) {
//                        Yii::$app->mailer->compose('company_add',
//                            [
//                                'slug' => $model->slug,
//                            ])
//                            ->setFrom([Yii::$app->params['fromEmail'] => Yii::t("app", "SITE_NAME")])
//                            ->setTo($userCompany->email)
//                            ->setCharset('utf8')
//                            ->setSubject(Module::t('app', 'Congratulations! Your business has been added to the UTEX directory.'))
//                            ->send();
//                    }
                }
              
                return $model;
            }
        }
        return false;
    }

    /**
     * @param $id
     * @return bool|CompanyForm
     */
    public static function findOne($id)
    {
        if (!empty($id)) {
            $company = Company::findOne(['id' => $id]);
            if($company) {
                $attributes = $company->attributes;
                if (!empty($attributes["contact_phone"])) {
                    $attributes["contact_phone"] = Json::decode($attributes["contact_phone"]);
                }
                $model = new self();
                if ($model->load($attributes, '')) {
                    $model->id = $company->id;
                    $model->name = $company->info->name;
                    $model->lang = $company->info->lang;
                    $companyTranslate = CompanyTranslate::find()->where(['catalogue_company_id' => $id])->all();
                    if($companyTranslate){
                        foreach ($companyTranslate as $item){
                            $model->translate[$item->lang] = $item;
                        }
                    }
                    $companySpecialization = CompanySpecialization::find()->where(['catalogue_company_id' => $id, 'main_specialization' => 1])->all();
                    if($companySpecialization){
                        $model->main_specialization = [];
                        foreach ($companySpecialization as $item){
                            $model->main_direction = $item->catalogue_direction_pid;
                            $model->check_specialization = $item->check_specialization;
                            array_push($model->main_specialization, $item->catalogue_specialization_pid);
                        }
                    }
                    $model->additional_specializations = self::getAdditional($model->id);
                    $model->check_additional_specialization = self::getCheckAdditional($model->id);

                    $companyEquipment = CompanyEquipment::find()->where(['catalogue_company_id' => $id])->all();
                    $model->equipment = [];
                    $model->check_equipment = [];
                    foreach ($companyEquipment as $item){
                        $model->equipment[$item->catalogue_equipment_pid] = $item->check_equipment;
                        $model->check_equipment[$item->catalogue_equipment_pid] = $item->check_equipment;
                    }
                    return $model;
                }
            }
        }
        return false;
    }

    /**
     * @param $companyId
     * @return array
     */
    private static function getAdditional($companyId)
    {
        if (!empty($companyId)) {
            $companySpecialization = CompanySpecialization::find()->where(['catalogue_company_id' => $companyId, 'main_specialization' => 0])->all();

            $result = [];
            foreach ($companySpecialization as $item) {
                $result[$item->catalogue_direction_pid][] = $item->catalogue_specialization_pid;
            }
            if (!empty($result)) return $result;
        }
        return [];
    }

    /**
     * @param $companyId
     * @return array
     */
    private static function getCheckAdditional($companyId)
    {
        if (!empty($companyId)) {
            $companySpecialization = CompanySpecialization::find()->where(['catalogue_company_id' => $companyId, 'main_specialization' => 0])->all();

            $result = [];
            foreach ($companySpecialization as $item) {
                $result[$item->catalogue_direction_pid] = $item->check_specialization;
            }
            if (!empty($result)) return $result;
        }
        return [];
    }

    /**
     * @param $array
     * @param $companyId
     */
    private function setAdditional($array, $companyId)
    {
        if (!empty($array)) {
            CompanySpecialization::deleteAll(['catalogue_company_id' => $companyId, 'main_specialization' => 0]);
            foreach ($array as $key => $item) {
                if (!empty($item['direction'])) {
                    if (empty($item['specialization'])) $item['specialization'] = [];
                    $item['specialization'] = array_diff($item['specialization'], array(''));
                    if (!empty($item['specialization'])) {
                        foreach ($item['specialization'] as $itemSpec) {
                            $modelSpecialization = new CompanySpecialization();
                            $modelSpecialization->catalogue_company_id = $companyId;
                            $modelSpecialization->catalogue_direction_pid = $item['direction'];
                            $modelSpecialization->catalogue_specialization_pid = $itemSpec;
                            $modelSpecialization->main_specialization = 0;
                            $modelSpecialization->check_specialization = $this->check_additional_specialization[$key] ?? 0;
                            $modelSpecialization->save();
                        }
                    } else {
                        $modelSpecialization = new CompanySpecialization();
                        $modelSpecialization->catalogue_company_id = $companyId;
                        $modelSpecialization->catalogue_direction_pid = $item['direction'];
                        $modelSpecialization->catalogue_specialization_pid = 0;
                        $modelSpecialization->main_specialization = 0;
                        $modelSpecialization->check_specialization = (int)$this->check_additional_specialization[$key] ?? 0;
                        $modelSpecialization->save();
                    }
                }
            }
        }
    }

}
