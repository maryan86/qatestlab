<?php

namespace backend\modules\catalogue\models;

use backend\modules\catalogue\enums\Status;
use backend\modules\catalogue\models\base\Catalogue;
use backend\modules\catalogue\Module;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "{{%catalogue_specialization}}".
 *
 * @property int $id
 * @property int $p_id
 * @property string $lang
 * @property string $name
 * @property string $status
 * @property int $direction_id
 * @property int $category_id
 * @property int $filter_id
 * @property int $created_at
 * @property int $updated_at
 */
class Specialization extends Catalogue
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%catalogue_specialization}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['p_id', 'direction_id', 'category_id', 'filter_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['lang', 'direction_id', 'category_id', 'name', 'status'], 'required'],
            [['lang'], 'string', 'max' => 3],
            [['name'], 'string', 'max' => 180],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'p_id' => Yii::t('app', 'P ID'),
            'lang' => Yii::t('app', 'Lang'),
            'name' => Module::t('app', 'Name Specialization'),
            'status' => Yii::t('app', 'Status'),
            'direction_id' => Module::t('app', 'Specialization direction'),
            'category_id' => Module::t('app', 'Specialization category'),
            'filter_id' => Module::t('app', 'Applies to the filter'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDirection()
    {
        return $this->hasOne(Direction::class, ['id' => 'direction_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::class, ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryFilter()
    {
        return $this->hasOne(CategoryFilter::class, ['id' => 'filter_id']);
    }

    /**
     * @param array $params
     * @param null $lang
     * @return ActiveDataProvider
     */
    public function searchDataProvider(array $params, $lang = null): ActiveDataProvider
    {
        $query = self::find()
            ->where(['lang' => $lang])
            ->andWhere(['in', 'status', [Status::PUBLISH, Status::DRAFT]]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => ['id', 'name', 'status', 'created_at', 'updated_at'],
                'defaultOrder' => ['created_at' => SORT_DESC, 'id' => SORT_DESC]
            ],
            'pagination' => [
                'pageSize' => $params['per-page'] ?? 50,
                'defaultPageSize' => 50
            ],
        ]);

        if (!$this->load($params)) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'like', 'name', $this->name,
        ]);

        $query->andFilterWhere([
            'direction_id' => $this->direction_id
        ]);

        $query->andFilterWhere([
            'category_id' => $this->category_id
        ]);

        return $dataProvider;
    }

    /**
     * @param array $params
     * @param string $lang
     * @return ActiveDataProvider
     */
    public static function getDataProvider(array $params, string $lang)
    {
        $specialization = new self();
        return $specialization->searchDataProvider($params, $lang);
    }

    /**
     * @param $directionId
     * @return \yii\db\ActiveQuery
     */
    public static function getSpecializationDirection($directionId){
        $direction = Direction::findOne(['p_id' => $directionId, 'lang' => Yii::$app->lang->get()]);
        return self::find()->where(['direction_id' => $direction->id, 'status' => Status::PUBLISH]);
    }

}
