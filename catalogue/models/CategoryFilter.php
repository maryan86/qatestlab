<?php

namespace backend\modules\catalogue\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%catalogue_category_filter}}".
 *
 * @property int $id
 * @property string $lang
 * @property string $name
 * @property int $catalogue_category_id
 * @property int $created_at
 * @property int $updated_at
 */
class CategoryFilter extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%catalogue_category_filter}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lang', 'name', 'catalogue_category_id'], 'required'],
            [['catalogue_category_id', 'created_at', 'updated_at'], 'integer'],
            [['lang'], 'string', 'max' => 3],
            [['name'], 'string', 'max' => 180],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => function () {
                    return date('U');
                },
            ]
        ];
    }

    /**
     * @param $category_id
     * @param $filtersIn
     * @param $lang
     * @return bool
     */
    public static function updateCategoryFilters($category_id, $filtersIn, $lang)
    {
        if (!empty($category_id)) {
            $filtersIs = self::find()->where(['catalogue_category_id' => $category_id])->all();
            $filtersInIds = $filtersIn['id'] ?? [];
            if ($filtersInIds) {
                foreach ($filtersInIds as $key => $item) {
                    self::updateAll(['name' => $item], ['id' => $key]);
                }
            }
            unset($filtersIn['id']);
            if ($filtersIn) {
                foreach ($filtersIn as $item) {
                    $modelCategoryFilter = new self();
                    $modelCategoryFilter->name = $item;
                    $modelCategoryFilter->lang = $lang;
                    $modelCategoryFilter->catalogue_category_id = $category_id;
                    $modelCategoryFilter->save();
                }
            }
            if (!empty($filtersIs)) {
                $filtersIs = ArrayHelper::getColumn($filtersIs, 'id');
                $diff_unset = array_values(array_diff($filtersIs, array_keys($filtersInIds)));
                if (!empty($diff_unset)) {
                    self::deleteAll(['and', ['catalogue_category_id' => $category_id], ['in', 'id', $diff_unset]]);
                }
            }
            return true;
        }
        return false;
    }

    /**
     * @param $category_id
     * @return array|ActiveRecord[]
     */
    public static function getCategoryFilter($category_id)
    {
        return self::find()->where(['catalogue_category_id' => $category_id])->all();
    }
}