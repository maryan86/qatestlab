<?php

namespace backend\modules\catalogue\models;

use backend\modules\catalogue\enums\Status;
use backend\modules\catalogue\models\base\Catalogue;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%catalogue_region}}".
 *
 * @property int $id
 * @property int $p_id
 * @property string $lang
 * @property string $name
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 */
class Region extends Catalogue
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%catalogue_region}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lang', 'name', 'status'], 'required'],
            [['p_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['lang'], 'string', 'max' => 3],
            [['name'], 'string', 'max' => 180],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'p_id' => Yii::t('app', 'P ID'),
            'lang' => Yii::t('app', 'Lang'),
            'name' => Yii::t('app', 'Name'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public static function getAll(): array
    {
        $lang = !empty(Yii::$app->modules['multilang']) ?
            Yii::$app->modules['multilang']->getDefaultLang()['url'] :
            (!empty(Yii::$app->params['defLang']) ? Yii::$app->params['defLang'] : "");
        $params = Yii::$app->request->queryParams;
        if (!empty($params['lang'])) {
            $lang = $params['lang'];
        }
        $regions = self::find()->where(['lang' => $lang])->andWhere(['in', 'status', [Status::PUBLISH, Status::DRAFT]])->all();
        if (!empty($regions)) {
            return ArrayHelper::map($regions, 'id', 'name');
        }
        return [];
    }
}
