<?php

namespace backend\modules\catalogue\models;

use backend\modules\catalogue\models\base\Catalogue;
use Yii;

/**
 * This is the model class for table "{{%catalogue_direction}}".
 *
 * @property int $id
 * @property int $p_id
 * @property string $lang
 * @property string $name
 * @property integer $image
 * @property string $status
 * @property int $created_at
 * @property int $updated_at
 */
class Direction extends Catalogue
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%catalogue_direction}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lang', 'name', 'status'], 'required'],
            [['p_id', 'image', 'status', 'created_at', 'updated_at'], 'integer'],
            [['lang'], 'string', 'max' => 3],
            [['name'], 'string', 'max' => 180],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'p_id' => Yii::t('app', 'P ID'),
            'lang' => Yii::t('app', 'Lang'),
            'name' => Yii::t('app', 'Name'),
            'image' => Yii::t('app', 'Images'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

}
