<?php

namespace backend\modules\catalogue\models;

use backend\modules\catalogue\enums\Status;
use backend\modules\catalogue\enums\StatusCompany;
use backend\modules\catalogue\models\CompanyTranslate;
use backend\modules\catalogue\Module;
use common\models\User;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

class CompanyReviews extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%catalogue_company_reviews}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => function () {
                    return date('U');
                },
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'company_id', 'created_at', 'updated_at'], 'integer'],
            [['user_id', 'company_id', 'review'], 'required'],
            [['review'], 'string'],
        ];
    }
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User'),
            'review' => Module::t('app', 'Company Review'),
            'company_id' => Module::t('app', 'Company Name'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * User Relations
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     *
     *  Company relations
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(CompanyTranslate::class, ['catalogue_company_id' => 'company_id']);
    }
}
