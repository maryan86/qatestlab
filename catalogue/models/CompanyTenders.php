<?php

namespace backend\modules\catalogue\models;
use backend\modules\catalogue\enums\Status;
use backend\modules\catalogue\enums\StatusTenders;
use backend\modules\catalogue\Module;
use frontend\modules\users\models\UserTendersActivity;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%company_tenders}}".
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property int $region_pid
 * @property int $capacity_min
 * @property int $capacity_max
 * @property int $capacity_type
 * @property int $purchase_amount
 * @property int $purchase_currency
 * @property int $proposal_to
 * @property string $description
 * @property string $attachment
 * @property string $name_customer
 * @property string $address_customer
 * @property string $contact_person
 * @property string $contact_phone
 * @property string $contact_email
 * @property int $created_at
 * @property int $updated_at
 *
 * @property User $user
 * @property TendersSpecialization[] $tendersSpecializations
 */
class CompanyTenders extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => function () {
                    return date('U');
                },
            ]
        ];
    }

    /**
     * @return \stdClass
     */
    public static function getData() {
        $data = new \stdClass();
        $data->lang = !empty(Yii::$app->modules['multilang']) ?
            Yii::$app->modules['multilang']->getDefaultLang()['url'] :
            (!empty(Yii::$app->params['defLang']) ? Yii::$app->params['defLang'] : "");
        return $data;
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%company_tenders}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['region_pid', 'quantity_type', 'purchase_currency','proposal_to'], 'integer'],
            [['purchase_amount','quantity_min', 'quantity_max'], 'number'],
            [
                [
                    'name',
                    'name_customer',
                    'address_customer',
                    'region_pid',
                    'quantity_min',
                    'quantity_max',
                    'quantity_type',
                    'purchase_amount',
                    'purchase_currency',
                    'proposal_to',
                    'contact_email',
                    'contact_person',
                    'contact_phone',
                    'quantity_min',
                    'quantity_max',
                    'quantity_type',
                    'purchase_amount',
                    'purchase_currency',
                    'proposal_to',
                    'description'
                ], 'required'],
            [['description'], 'string', 'max' => 2000],
            [['name', 'attachment', 'name_customer', 'address_customer'], 'string', 'max' => 255],
            [['contact_person'], 'string', 'max' => 100],
            [['contact_phone'], 'string', 'max' => 20],
            [['contact_email'], 'email'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => Yii::t('app', 'User'),
            'name' => Module::t('app','Title, keywords'),
            'region_pid' => Module::t('app', 'Region'),
            'quantity_min' => Module::t('app', 'Quantity'),
            'quantity_max' => Module::t('app', 'Quantity'),
            'quantity_type' => Module::t('app', 'Quantity Type'),
            'purchase_amount' => Module::t('app','Purchase amount'),
            'purchase_currency' => Module::t('app','Currency'),
            'proposal_to' => Module::t('app','Filing deadline'),
            'description' => Module::t('app','Additional Information'),
            'attachment' => Module::t('app','Attachment'),
            'name_customer' => Module::t('app','Customer name'),
            'address_customer' => Module::t('app','Customer address'),
            'contact_person' => Module::t('app', 'Contact Person'),
            'contact_email' => Module::t('app', 'Contact E-mail'),
            'contact_phone' => Module::t('app', 'Contact Phone'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Region::class, ['p_id' => 'region_pid'])
            ->onCondition(['lang' => Yii::$app->lang->get(),'status'=>Status::PUBLISH]);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpecialization()
    {
        return $this->hasMany(TendersSpecialization::class, ['tenders_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDirection()
    {
        return $this->hasOne(TendersSpecialization::class, ['tenders_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivityUser()
    {
        return $this->hasOne(UserTendersActivity::class, ['tender_id' => 'id'])->onCondition([UserTendersActivity::tableName().'.user_id' => Yii::$app->user->identity->id]);
    }

    /**
     * @param array $specializationIDs
     * @return \yii\db\ActiveQuery|null
     */
    public static function getAllTenders(array $specializationIDs, array $andFilterWhere, array $orderBy = ['created_at' => SORT_DESC]) : ActiveQuery {

        $query = self::find()
            ->joinWith('specialization')
            ->joinWith('activityUser')
            ->where([self::tableName().'.status' => StatusTenders::ACTIVE])
            ->andWhere(
                ['or',
                    ['IN',TendersSpecialization::tableName() . '.tenders_direction_pid', $specializationIDs],
                    [self::tableName().'.user_id'=> Yii::$app->user->identity->id],
                ])
            ->andFilterWhere($andFilterWhere)
            ->distinct()
            ->orderBy($orderBy);
        return $query ? $query : null;
    }


    /**
     * @param array $specializationIDs
     * @return \yii\db\ActiveQuery|null
     */
    public static function getUserTenders(array $orderBy = ['created_at' => SORT_DESC]) : ActiveQuery {
        $query = self::find()
            ->where([self::tableName().'.status' => StatusTenders::ACTIVE])
            ->andWhere(['user_id'=> Yii::$app->user->identity->id])
            ->orderBy($orderBy);
        return $query ? $query : null;
    }

    /**
     * @param array $specializationIDs
     * @return \yii\db\ActiveQuery|null
     */
    public static function getCountNotReadTenders($specializationIDs): int
    {
        $query = self::find()->joinWith('activityUser')->joinWith('specialization')
            ->where([self::tableName().'.status' => StatusTenders::ACTIVE])
            ->andWhere(
                ['or',
                    ['IN',TendersSpecialization::tableName() . '.tenders_direction_pid', $specializationIDs],
                    [self::tableName().'.user_id'=> Yii::$app->user->identity->id],
                ])
            ->andFilterWhere(['or',
                [UserTendersActivity::tableName() . '.reading' => 0],
                ['is', UserTendersActivity::tableName() . '.reading', new \yii\db\Expression('null')],
            ])
            ->distinct()
            ->count();
        return $query ? $query : 0;
    }

    /**
     * @param array $where
     * @param array $andWhere
     * @param bool $joinWith
     * @param int $limit
     * @return array|ActiveRecord[]
     */
    public static function getListTenders(array $where = [], array $andWhere = [], bool $joinWith = false, int $limit = 3) {

        $query = self::find();
        if($joinWith === true) {
            $query->joinWith('specialization');
        }
        $query->where(['>=', self::tableName().'.proposal_to', strtotime("-7 day")])
                ->andWhere([self::tableName().'.status' => StatusTenders::ACTIVE])
                ->andWhere($where)
                ->andFilterWhere($andWhere)
                ->limit($limit)
                ->orderBy([self::tableName().'.proposal_to'=> SORT_ASC]);
        return $query->all();
    }

    /**
     * @param $id
     * @return array|ActiveRecord|null
     */
    public static function get($id)  {
        $query = self::find()
            ->where([self::tableName().'.id' => $id])
            ->andWhere([self::tableName().'.status' => StatusTenders::ACTIVE])
            ->one();
        return $query;
    }

    /**
     * @param int $p_id
     * @return ActiveRecord
     */
    public static function getDirectionItem(int $p_id)
    {
        $model = Direction::find()
            ->where(['p_id' => $p_id, 'lang' => Yii::$app->lang->get()])
            ->andWhere(['status'=>Status::PUBLISH])
            ->one();
        return $model ? $model : [];
    }


    /**
     * @param array $p_ids
     * @return array|ActiveRecord[]
     */
    public static function getSpecializationItems(array $p_ids)
    {
        $model = Specialization::find()
            ->where(['lang' => Yii::$app->lang->get()])
            ->andWhere(['status'=>Status::PUBLISH])
            ->andWhere(['IN','p_id', $p_ids ])
            ->all();
        return $model ? $model : [];
    }
}