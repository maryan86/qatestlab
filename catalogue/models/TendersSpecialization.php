<?php

namespace backend\modules\catalogue\models;
use Yii;

/**
 * This is the model class for table "{{%tenders_specialization}}".
 *
 * @property int $id
 * @property int $tenders_id
 * @property int $tenders_direction_pid
 * @property int $tenders_specialization_pid
 * @property int $created_at
 * @property int $updated_at
 *
 * @property CompanyTenders $tenders
 */
class TendersSpecialization extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%company_tenders_specialization}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tenders_id', 'tenders_direction_pid', 'tenders_specialization_pid'], 'required'],
            [['tenders_id', 'tenders_direction_pid', 'tenders_specialization_pid', 'created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tenders_id' => 'Tenders ID',
            'tenders_direction_pid' => 'Tenders Direction Pid',
            'tenders_specialization_pid' => 'Tenders Specialization Pid',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenders()
    {
        return $this->hasOne(CompanyTenders::class, ['id' => 'tenders_id']);
    }
}
