<?php

namespace backend\modules\catalogue\models;

use backend\modules\catalogue\enums\Status;
use backend\modules\catalogue\enums\StatusCompany;
use backend\modules\catalogue\Module;
use backend\modules\catalogue\models\Region;
use backend\modules\catalogue\models\CompanySpecialization;
use backend\modules\catalogue\models\Specialization;
use backend\modules\catalogue\models\Equipment;
use backend\modules\catalogue\models\CompanyEquipment;
use common\models\User;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%catalogue_company}}".
 *
 * @property int $id
 * @property string $email
 * @property string $edrpou
 * @property string $site
 * @property string $phone
 * @property int $year
 * @property string $contact_person
 * @property string $contact_email
 * @property string $contact_phone
 * @property int $region_pid
 * @property string $logo
 * @property string $photos
 * @property int $capacity_min
 * @property int $capacity_max
 * @property int $capacity_type
 * @property int $personnel_quantity
 * @property int $tailor_made
 * @property int $bank_payment
 * @property int $cash_payment
 * @property int $vat_payment
 * @property int $verified
 * @property int $associated
 * @property int $check_location
 * @property int $check_personnel_quantity
 * @property int $check_year
 * @property int $check_site
 * @property int $check_capacity
 * @property int $status
 * @property int $date_verified
 * @property int $user_id
 * @property int $created_at
 * @property int $updated_at
 * @property int $main_direction
 * @property int $main_specialization
 * @property int $additional_directions
 * @property int $additional_specializations
 */
class Company extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%catalogue_company}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => function () {
                    return date('U');
                },
            ],
            [
                'class' => \backend\components\SluggableBehavior::class,
                'attribute' => null,
                'value' => function ($event) {
                    $name = CompanyTranslate::findOne(['catalogue_company_id' => $this->id])->name ?? $this->id;
                    $slug = \yii\helpers\Inflector::slug($name, '-');
                    $isSlug = self::find()
                        ->where(['slug' => $slug])
                        ->andWhere(['<>', 'id', $this->id])
                        ->one();
                    if (!empty($isSlug)) return $slug . '-' . $this->id;
                    return $slug;
                },
                'slugAttribute' => 'slug',
                'transliterator' => 'Ukrainian-Latin/BGN; NFKD',
                //Set this to true, if you want to update a slug when source attribute has been changed
                'forceUpdate' => true
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['year', 'edrpou', 'region_pid', 'capacity_min', 'capacity_max', 'capacity_type', 'personnel_quantity', 'tailor_made', 'bank_payment', 'cash_payment', 'vat_payment', 'verified', 'associated', 'check_location', 'check_personnel_quantity', 'check_year', 'check_site', 'check_capacity', 'status', 'user_id', 'date_verified', 'created_at', 'updated_at'], 'integer'],
            [['photos'], 'string'],
            [['status'], 'required'],
            //[['slug'], 'unique'],
            [['email', 'contact_email'], 'string', 'max' => 50],
            [['site', 'contact_person'], 'string', 'max' => 100],
            [['phone'], 'string', 'max' => 20],
            [['contact_phone'], 'string', 'max' => 255],
            [['logo'], 'string', 'max' => 255],
            [['edrpou'], 'string', 'length' => 8],
            ['edrpou', 'match', 'pattern' => '/^\d{8}$/', 'message' => Yii::t('app', 'Field must contain exactly 8 digits.')],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'email' => Module::t('app', 'Company E-mail'),
            'name' => Module::t('app', 'Company Name'),
            'edrpou' => Module::t('app', 'Edrpou code'),
            'site' => Module::t('app', 'Site'),
            'phone' => Module::t('app', 'Company Phone'),
            'year' => Module::t('app', 'How old is the company'),
            'description' => Module::t('app', 'A brief description of the company'),
            'contact_person' => Module::t('app', 'Contact Person'),
            'contact_email' => Module::t('app', 'Contact E-mail'),
            'contact_phone' => Module::t('app', 'Contact Phone'),
            'region_pid' => Module::t('app', 'Region'),
            'city' => Module::t('app', 'City'),
            'logo' => Module::t('app', 'Company logo'),
            'photos' => Module::t('app', 'Photo production'),
            'capacity_min' => Module::t('app', 'Min'),
            'capacity_max' => Module::t('app', 'Max'),
            'capacity_type' => Module::t('app', 'Capacity Type'),
            'personnel_quantity' => Module::t('app', 'Personnel Quantity'),
            'tailor_made' => Module::t('app', 'Tailoring from giving materials'),
            'bank_payment' => Module::t('app', 'Cash payment'),
            'cash_payment' => Module::t('app', 'Non-cash payment'),
            'vat_payment' => Module::t('app', 'Payment for VAT'),
            'verified' => Module::t('app', 'Verified Profile'),
            'associated' => Module::t('app', 'Associated'),
            'check_location' => Module::t('app', 'Check Location'),
            'check_personnel_quantity' => Module::t('app', 'Check Personnel Quantity'),
            'check_year' => Module::t('app', 'Check Year'),
            'check_site' => Module::t('app', 'Check Site'),
            'check_capacity' => Module::t('app', 'Check Capacity'),
            'status' => Module::t('app', 'Status'),
            'user_id' => Yii::t('app', 'User'),
            'date_verified' => Module::t('app', 'Date added to the directory'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return CompanyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CompanyQuery(get_called_class());
    }

    /**
     * @return array|Company[]|null
     */
    public static function getListCompanyUser()
    {
        $query = self::find()->joinWith('info')
            ->where(['lang' => Yii::$app->lang->get(), 'user_id' => Yii::$app->user->identity->id])
            ->andWhere(['in', 'status', [StatusCompany::VERIFY, StatusCompany::NOT_COMPLETED, StatusCompany::TO_CHECK]])
            ->orderBy('created_at DESC')
            ->all();

        return $query ? $query : null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInfo()
    {
        return $this->hasOne(CompanyTranslate::class, ['catalogue_company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Region::class, ['p_id' => 'region_pid'])->where([Region::tableName() . '.lang' => $this->info->lang]);
    }

    /**
     * User relations
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return array
     */
    public function getInfoLanguages(): array
    {
        $info = [];
        $query = $this->hasMany(CompanyTranslate::class, ['catalogue_company_id' => 'id'])->all();
        if($query) {
            foreach ($query as $item) {
                $info[$item->lang] = $item;
            }
        }
        return $info;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpecialization()
    {
        return $this->hasOne(CompanySpecialization::class, ['catalogue_company_id' => 'id'])->andOnCondition(['main_specialization' => '1']);
    }

    /**.
     * @return array|Company[]|null
     */
    public static function getCompanyAndSpecialization()
    {
        $query = self::find()->joinWith('specialization')
            ->where(['user_id' => Yii::$app->user->identity->id])
            ->andWhere(['in', 'status', [StatusCompany::VERIFY]])
            ->orderBy('created_at DESC')
            ->all();

        return $query ? $query : null;
    }

    /**
     * @return array
     */
    public function getSpecializationSpecialOrder(): array
    {
        $result = CompanySpecialization::find()->joinWith(['specialization'])
            ->where([Specialization::tableName() . '.lang' => $this->info->lang, Specialization::tableName() . '.status' => Status::PUBLISH, 'catalogue_company_id' => $this->id])
            ->andWhere(['<>', Specialization::tableName() . '.category_id', 0])
            ->orderBy([CompanySpecialization::tableName() . '.main_specialization' => SORT_DESC, Specialization::tableName() . '.name' => SORT_ASC])->all();
        if (!empty($result)) return $result;
        return [];
    }

    /**
     * @return array
     */
    public function getMainSpecializationLineSpecialOrder(): array
    {
        $result = CompanySpecialization::find()->joinWith(['specialization'])
            ->where([Specialization::tableName() . '.lang' => $this->info->lang,
                Specialization::tableName() . '.status' => Status::PUBLISH, 'catalogue_company_id' => $this->id,
                CompanySpecialization::tableName() . '.main_specialization' => 1])
            ->andWhere(['<>', Specialization::tableName() . '.category_id', 0])
            ->orderBy([Specialization::tableName() . '.name' => SORT_ASC])->all();
        if (!empty($result)) {
            $resultNames = ArrayHelper::getColumn($result, 'specialization.name');
            return ['text' => implode(', ', $resultNames), 'check' => $result[0]->check_specialization];
        }
        return [];
    }

    /**
     * @return array
     */
    public function getEquipmentSpecialOrder(): array
    {
        $result = CompanyEquipment::find()->joinWith(['equipment'])
            ->where([Equipment::tableName() . '.lang' => $this->info->lang, Equipment::tableName() . '.status' => Status::PUBLISH, 'catalogue_company_id' => $this->id])
            ->orderBy([Equipment::tableName() . '.name' => SORT_ASC])->all();
        if (!empty($result)) return $result;
        return [];

    }
}
