<?php

namespace backend\modules\catalogue\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%catalogue_company_specialization}}".
 *
 * @property int $id
 * @property int $catalogue_company_id
 * @property int $catalogue_direction_pid
 * @property int $catalogue_specialization_pid
 * @property int $main_specialization
 * @property int $check_specialization
 * @property int $created_at
 * @property int $updated_at
 */
class CompanySpecialization extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%catalogue_company_specialization}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => function () {
                    return date('U');
                },
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['catalogue_company_id', 'catalogue_direction_pid', 'catalogue_specialization_pid'], 'required'],
            [['catalogue_company_id', 'catalogue_direction_pid', 'catalogue_specialization_pid', 'main_specialization', 'check_specialization', 'created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'catalogue_company_id' => Yii::t('app', 'Catalogue Company ID'),
            'catalogue_direction_pid' => Yii::t('app', 'Catalogue Direction Pid'),
            'catalogue_specialization_pid' => Yii::t('app', 'Catalogue Specialization Pid'),
            'main_specialization' => Yii::t('app', 'Main Specialization'),
            'check_specialization' => Yii::t('app', 'Check Specialization'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function getSpecialization()
    {
        return $this->hasOne(Specialization::class, ['p_id' => 'catalogue_specialization_pid']);
    }
}
