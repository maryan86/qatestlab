<?php

namespace backend\modules\catalogue\models;

use backend\modules\catalogue\models\base\Catalogue;
use backend\modules\catalogue\Module;
use Yii;

/**
 * This is the model class for table "{{%catalogue_category}}".
 *
 * @property int $id
 * @property int $p_id
 * @property string $lang
 * @property int $direction_id
 * @property string $name
 * @property string $status
 * @property int $created_at
 * @property int $updated_at
 */
class Category extends Catalogue
{
    /**
     * @var array
     */
    public $filter = [];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%catalogue_category}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['p_id', 'direction_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['lang', 'name', 'status'], 'required'],
            [['lang'], 'string', 'max' => 3],
            [['name'], 'string', 'max' => 180],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'p_id' => Yii::t('app', 'P ID'),
            'lang' => Yii::t('app', 'Lang'),
            'direction_id' => Module::t('app', 'Direction'),
            'name' => Module::t('app', 'Category Name'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'filter' => Yii::t('app', 'Filter'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDirection()
    {
        return $this->hasOne(Direction::class, ['id' => 'direction_id']);
    }

    /**
     * @param $direction_id
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getCategoryDirection($direction_id)
    {
        return self::find()->where(['direction_id' => $direction_id])->all();
    }
}
