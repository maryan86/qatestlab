<?php

namespace backend\modules\catalogue\models\base;

use \yii\db\ActiveRecord;
use \yii\behaviors\TimestampBehavior;

/**
 * Class Catalogue
 * @package backend\modules\catalogue\models
 */
class Catalogue extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => function () {
                    return date('U');
                },
            ]
        ];
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if (!$this->p_id) {
            $this->p_id = $this->id;
            $this->update();
        }
    }

    /**
     * @param array $request
     * @param int $id
     * @return bool
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\StaleObjectException
     */
    public static function updateOne(array $request, int $id): bool
    {
        $personal = new self();
        $personal = self::findOne(['p_id' => $request[$personal->formName()]['p_id'], 'lang' => $request[$personal->formName()]['lang']]);
        if ($personal == null) return self::create($request);
        if ($personal->load($request)) {
            return $personal->update();
        }
        return false;
    }

    /**
     * @param array $request
     * @return bool
     */
    public static function create(array $request)
    {
        $model = new static();
        if ($model->load($request)) {
            return $model->save();
        }
        return false;
    }

    /**
     * @param int $p_id
     * @param string $lang
     * @return Catalogue|null
     */
    public static function get(int $p_id, string $lang)
    {
        $model = static::findOne(['p_id' => $p_id, 'lang' => $lang]);
        if ($model == null) {
            $model = new static();
            $model->setAttribute('p_id', $p_id);
            $model->setAttribute('lang', $lang);
        };
        return $model;
    }

}