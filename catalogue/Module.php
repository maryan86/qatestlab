<?php

namespace backend\modules\catalogue;

use Yii;
use yii\base\BootstrapInterface;


/**
 * catalogue module definition class
 */
class Module extends \yii\base\Module implements BootstrapInterface
{
    /**
     * default controller namespace.
     * @var string
     */
    public $controllerNamespace = 'backend\modules\catalogue\controllers';

    /**
     * default controller.
     * @var string
     */

    public $defaultController = 'catalogue';
    /**
     * default route.
     * @var string
     */
    public $defaultRoute = 'catalogue';

    /**
     * Initializes modules catalogue.
     */
    public function init()
    {
        \Yii::$app->mailer->viewPath = '@backend/modules/catalogue/views/mail';
        parent::init();
        $this->registerTranslations();
    }

    /**
     * @param \yii\base\Application $app
     */
    public function bootstrap($app)
    {
        $app->urlManager->addRules([
            /**
             * direction rules
             */
            [
                'pattern' => '<lang:[\w\-]+>/<module:catalogue>/direction/<action>/<id:\d+>',
                'route' => '<module>/direction/<action>'
            ],
            [
                'pattern' => '<module:catalogue>/direction/<action>/<id:\d+>',
                'route' => '<module>/direction/<action>'
            ],
            [
                'pattern' => '<lang:[\w\-]+>/<module:catalogue>/direction/<action>',
                'route' => '<module>/direction/<action>'
            ],
            [
                'pattern' => '<module:catalogue>/direction/<action>',
                'route' => '<module>/direction/<action>'
            ],
            [
                'pattern' => '<lang:[\w\-]+>/<module:catalogue>/direction',
                'route' => '<module>/direction/index'
            ],
            [
                'pattern' => '<module:catalogue>/direction',
                'route' => '<module>/direction/index'
            ],

            /**
             * specialization rules
             */
            [
                'pattern' => '<lang:[\w\-]+>/<module:catalogue>/specialization/<action>/<id:\d+>',
                'route' => '<module>/specialization/<action>'
            ],
            [
                'pattern' => '<module:catalogue>/specialization/<action>/<id:\d+>',
                'route' => '<module>/specialization/<action>'
            ],
            [
                'pattern' => '<lang:[\w\-]+>/<module:catalogue>/specialization/<action>',
                'route' => '<module>/specialization/<action>'
            ],
            [
                'pattern' => '<module:catalogue>/specialization/<action>',
                'route' => '<module>/specialization/<action>'
            ],
            [
                'pattern' => '<lang:[\w\-]+>/<module:catalogue>/specialization',
                'route' => '<module>/specialization/index'
            ],
            [
                'pattern' => '<module:catalogue>/specialization',
                'route' => '<module>/specialization/index'
            ],

            /**
             * category rules
             */
            [
                'pattern' => '<lang:[\w\-]+>/<module:catalogue>/category/<action>/<id:\d+>',
                'route' => '<module>/category/<action>'
            ],
            [
                'pattern' => '<module:catalogue>/category/<action>/<id:\d+>',
                'route' => '<module>/category/<action>'
            ],
            [
                'pattern' => '<lang:[\w\-]+>/<module:catalogue>/category/<action>',
                'route' => '<module>/category/<action>'
            ],
            [
                'pattern' => '<module:catalogue>/category/<action>',
                'route' => '<module>/category/<action>'
            ],
            [
                'pattern' => '<lang:[\w\-]+>/<module:catalogue>/category',
                'route' => '<module>/category/index'
            ],
            [
                'pattern' => '<module:catalogue>/category',
                'route' => '<module>/category/index'
            ],

            /**
             * region rules
             */
            [
                'pattern' => '<lang:[\w\-]+>/<module:catalogue>/region/<action>/<id:\d+>',
                'route' => '<module>/region/<action>'
            ],
            [
                'pattern' => '<module:catalogue>/region/<action>/<id:\d+>',
                'route' => '<module>/region/<action>'
            ],
            [
                'pattern' => '<lang:[\w\-]+>/<module:catalogue>/region/<action>',
                'route' => '<module>/region/<action>'
            ],
            [
                'pattern' => '<module:catalogue>/region/<action>',
                'route' => '<module>/region/<action>'
            ],
            [
                'pattern' => '<lang:[\w\-]+>/<module:catalogue>/region',
                'route' => '<module>/region/index'
            ],
            [
                'pattern' => '<module:catalogue>/region',
                'route' => '<module>/region/index'
            ],

            /**
             * Equipment rules
             */
            [
                'pattern' => '<lang:[\w\-]+>/<module:catalogue>/equipment/<action>/<id:\d+>',
                'route' => '<module>/equipment/<action>'
            ],
            [
                'pattern' => '<module:catalogue>/equipment/<action>/<id:\d+>',
                'route' => '<module>/equipment/<action>'
            ],
            [
                'pattern' => '<lang:[\w\-]+>/<module:catalogue>/equipment/<action>',
                'route' => '<module>/equipment/<action>'
            ],
            [
                'pattern' => '<module:catalogue>/equipment/<action>',
                'route' => '<module>/equipment/<action>'
            ],
            [
                'pattern' => '<lang:[\w\-]+>/<module:catalogue>/equipment',
                'route' => '<module>/equipment/index'
            ],
            [
                'pattern' => '<module:catalogue>/equipment',
                'route' => '<module>/equipment/index'
            ],

            /**
             * Companies rules
             */
            [
                'pattern' => '<lang:[\w\-]+>/<module:catalogue>/companies/<action>/<id:\d+>',
                'route' => '<module>/companies/<action>'
            ],
            [
                'pattern' => '<module:catalogue>/companies/<action>/<id:\d+>',
                'route' => '<module>/companies/<action>'
            ],
            [
                'pattern' => '<lang:[\w\-]+>/<module:catalogue>/companies/<action>',
                'route' => '<module>/companies/<action>'
            ],
            [
                'pattern' => '<module:catalogue>/companies/<action>',
                'route' => '<module>/companies/<action>'
            ],
            [
                'pattern' => '<lang:[\w\-]+>/<module:catalogue>/companies',
                'route' => '<module>/companies/index'
            ],
            [
                'pattern' => '<module:catalogue>/companies',
                'route' => '<module>/companies/index'
            ],


            /**
             * Companies rules
             */
            [
                'pattern' => '<lang:[\w\-]+>/<module:catalogue>/tenders/<action>/<id:\d+>',
                'route' => '<module>/tenders/<action>'
            ],
            [
                'pattern' => '<module:catalogue>/tenders/<action>/<id:\d+>',
                'route' => '<module>/tenders/<action>'
            ],
            [
                'pattern' => '<lang:[\w\-]+>/<module:catalogue>/tenders/<action>',
                'route' => '<module>/tenders/<action>'
            ],
            [
                'pattern' => '<module:catalogue>/tenders/<action>',
                'route' => '<module>/tenders/<action>'
            ],
            [
                'pattern' => '<lang:[\w\-]+>/<module:catalogue>/tenders',
                'route' => '<module>/tenders/index'
            ],
            [
                'pattern' => '<module:catalogue>/tenders',
                'route' => '<module>/tenders/index'
            ],
            /**
             * Companies rules
             */
            [
                'pattern' => '<lang:[\w\-]+>/<module:catalogue>/reviews/<action>/<id:\d+>',
                'route' => '<module>/reviews/<action>'
            ],
            [
                'pattern' => '<module:catalogue>/reviews/<action>/<id:\d+>',
                'route' => '<module>/reviews/<action>'
            ],
            [
                'pattern' => '<lang:[\w\-]+>/<module:catalogue>/reviews/<action>',
                'route' => '<module>/reviews/<action>'
            ],
            [
                'pattern' => '<module:catalogue>/reviews/<action>',
                'route' => '<module>/reviews/<action>'
            ],
            [
                'pattern' => '<lang:[\w\-]+>/<module:catalogue>/reviews',
                'route' => '<module>/reviews/index'
            ],
            [
                'pattern' => '<module:catalogue>/reviews',
                'route' => '<module>/reviews/index'
            ],
            /**
             * upload rules
             */
            [
                'pattern' => '<lang:[\w\-]+>/<module:catalogue>/upload/<action>',
                'route' => '<module>/upload/<action>'
            ],
            [
                'pattern' => '<module:catalogue>/upload/<action>',
                'route' => '<module>/upload/<action>'
            ],
            [
                'pattern' => '<lang:[\w\-]+>/<module:catalogue>/upload',
                'route' => '<module>/upload/index'
            ],
            [
                'pattern' => '<module:catalogue>/upload',
                'route' => '<module>/upload/index'
            ],
            /**
             * general rules
             */
            [
                'pattern' => '<lang:[\w\-]+>/<module:catalogue>/<action>',
                'route' => '<module>/catalogue/<action>'
            ],
            [
                'pattern' => '<lang:[\w\-]+>/<module:catalogue>/<action>/<id:\d+>',
                'route' => '<module>/catalogue/<action>'
            ],
            [
                'pattern' => '<lang:[\w\-]+>/<module>/catalogue/<action>',
                'route' => 'site/error'
            ],
            [
                'pattern' => '<module:catalogue>/<action>',
                'route' => '<module>/catalogue/<action>'
            ],
            [
                'pattern' => '<module:catalogue>/<action>/<id:\d+>',
                'route' => '<module>/catalogue/<action>'
            ],
            [
                'pattern' => '<module>/catalogue/<action>',
                'route' => 'site/error'
            ],

        ], false);
    }

    /**
     * Register transliteration module catalogue.
     *
     */
    public function registerTranslations()
    {
        Yii::$app->i18n->translations['modules/catalogue/*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => Yii::$app->sourceLanguage,
            'basePath' => '@modules/catalogue/messages',
            'fileMap' => [
                'modules/catalogue/app' => 'app.php',
                'modules/catalogue/app/validation' => 'validation.php',
            ],
        ];
    }

    /**
     * Overwrite standard function  Yii::t() usage Module::t()
     *
     * @param string $category the message category.
     * @param string $message the message to be translated.
     * @param array $params the parameters that will be used to replace the corresponding placeholders in the message.
     * @param string $language the language code (e.g. `en-US`, `en`).
     * If this is null, the current [[\yii\base\Application::language|application language]] will be used.
     *
     * @return string the translated message.
     */
    public static function t($category, $message, $params = [], $language = null)
    {
        return Yii::t('modules/catalogue/' . $category, $message, $params, $language);
    }

}
